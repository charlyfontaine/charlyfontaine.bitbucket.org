// Search controller that we use whenever we have a search inputs
// and search results
dinnerPlannerApp.controller('SearchCtrl', function ($scope,Dinner) {
  //$scope.dishType = 'meal';
  $scope.dishTypefunc = function(varDishType){
    if((varDishType=='meal')||(varDishType=='meal')&&(query==undefined)){console.log('meal isnt ok');Dinner.dishOk=1;}
    Dinner.dishType = varDishType;
    $scope.dishType = varDishType;
  }
	$scope.search = function(query, dishType) {
    dishType = Dinner.dishType;
    console.log(query, dishType);
    if ((dishType !='meal')){ //the dish is a SMD 
      Dinner.dishOk = 0;
      $scope.status = "Searching...";
       Dinner.DishSearch.get({title_kw:query, any_kw:dishType},function(data){
       $scope.dishes=data.Results;
       $scope.status = "Showing " + data.Results.length + " results";
     },function(data){
       $scope.status = "There was an error";
     });
   }
    else{
      console.log('dish isnt a SMD');
      Dinner.dishOk = 1;
      $scope.dishType = 'meal';
      console.log(query);
        $scope.status = "Searching...";
       Dinner.DishSearch.get({title_kw:query},function(data){
       $scope.dishes=data.Results;
       $scope.status = "Showing " + data.Results.length + " results";
     },function(data){
       $scope.status = "There was an error";
     });
    }
 }
$scope.updatePrice= function(){
  console.log("price updated");
}
$scope.checkMenu = function(){
  if ((Dinner.dishIdArray.saladId == 0 )||(Dinner.dishIdArray.mainId == 0 )||(Dinner.dishIdArray.dessertId == 0 )){
    alert(" You must select the entire meal ")
    return $scope.href = "#/search"
  }
  else { return $scope.href = "#/mymeal" }

}
$scope.starter = "red";
$scope.main = "red";
$scope.dessert = "red";
if (Dinner.dishIdArray.saladId != 0){$scope.starter = "green";}
if (Dinner.dishIdArray.mainId != 0){$scope.main = "green";}
if (Dinner.dishIdArray.dessertId != 0){$scope.dessert = "green";}
  // $scope.meal = function(){
  //   console.log(Dinner.dishArrayCookie.SaladObject);
  //   console.log(Dinner.dishArrayCookie.mainDishObject);
  //   console.log(Dinner.dishArrayCookie.dessertObject);
  //   //$scope.yourMeal = Dinner.dishObjectArray.mainDishObject.Title;
  // }
  // TODO in Lab 5: you will need to implement a method that searches for dishes
  // including the case while the search is still running.

});