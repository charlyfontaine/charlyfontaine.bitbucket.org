// Dinner controller that we use whenever we want to display detailed
// information for one dish
dinnerPlannerApp.controller('DishCtrl', function ($scope,$routeParams,Dinner) {

	var meal;
 	$scope.selectedDish = Dinner.Dish.get({id:$routeParams.dishId},function(detail){
 		meal = detail;
        console.log("meal.RecipeID " + meal.RecipeID + "Dinner.dishIdArray.saladId " + Dinner.dishIdArray.saladId);
        if((meal.RecipeID==Dinner.dishIdArray.saladId)||(meal.RecipeID==Dinner.dishIdArray.maiId)||(meal.RecipeID==Dinner.dishIdArray.dessertId)){
        $scope.Selected = "Selected";
        console.log("dish selected");
        }
        $scope.mealPrice = 0;
        for ( i = 0; i < meal.Ingredients.length; i++){
            $scope.mealPrice += meal.Ingredients[i].Quantity;
        }
        console.log($scope.mealPrice);
 	});
 	
 	//$scope.selectedDishIngredient = Dinner.Dish.get({id:$routeParams.dishId});
  // TODO in Lab 5: you need to get the dish according to the routing parameter
  // $routingParams.paramName
  // Check the app.js to figure out what is the paramName in this case
    $scope.addMeal = function(){
    if (Dinner.dishOk==0){
        if (Dinner.dishType=='Salad'){
            Dinner.dishObjectArray.SaladObject = meal;
            Dinner.dishIdArray.saladId = meal.RecipeID;
            console.log(Dinner.dishObjectArray.SaladObject.Title);
        }
        if (Dinner.dishType=='Main Dish'){
            Dinner.dishObjectArray.mainDishObject = meal;
            Dinner.dishIdArray.mainId = meal.RecipeID;
            console.log(Dinner.dishObjectArray.mainDishObject.Title);
        }
        if (Dinner.dishType=='Desserts'){
            Dinner.dishObjectArray.dessertObject = meal;
            Dinner.dishIdArray.dessertId = meal.RecipeID;
            console.log(Dinner.dishObjectArray.dessertObject.Title);
        }
        Dinner.updateMealCookie();
        yourMeal = meal;
        console.log('meal ok');
        $scope.Selected = "Selected";
    }
    else{alert('Select the type of the dish');}
  }

});