// Dinner controller that we use whenever we have view that needs to 
// display or modify the dinner menu
dinnerPlannerApp.controller('DinnerCtrl', function ($scope,Dinner) {
 
  $scope.numberOfGuests = Dinner.getNumberOfGuests();

  $scope.setNumberOfGuest = function(number){
    Dinner.setNumberOfGuests(number);
   	Dinner.updateMeal();
   	$scope.updatePrice();
  }

  $scope.getNumberOfGuests = function() {
    return Dinner.getNumberOfGuests();
  }


});