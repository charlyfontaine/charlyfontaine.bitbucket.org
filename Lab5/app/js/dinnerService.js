// Here we create an Angular service that we will use for our 
// model. In your controllers (or other services) you can include the
// dependency on any service you need. Angular will insure that the
// service is created first time it is needed and then just reuse it
// the next time.
dinnerPlannerApp.factory('Dinner',function ($resource, $cookieStore) {
  var numberOfGuest;
  var numberOfGuestCookie = $cookieStore.get('numberOfGuest');
  
  if(numberOfGuestCookie != undefined ){ //not the first load of the page
    console.log('not init cookie load');
    numberOfGuest=numberOfGuestCookie;
  }
  else{ //first load of the page, number of guest and cookie init at 2
    numberOfGuest=2;
    $cookieStore.put('numberOfGuest',numberOfGuest);
    numberOfGuestCookie = $cookieStore.get('numberOfGuest');
    console.log('init');

  }
  this.setNumberOfGuests = function(num) {
    if (num > 0 && num <= 42){
      numberOfGuest = num;
      $cookieStore.put('numberOfGuest',numberOfGuest);
      console.log(numberOfGuest);
    }
    else {alert("Guest number out of range");}
  }

  this.getNumberOfGuests = function() {
    $cookieStore.put('numberOfGuest',numberOfGuest);
    return numberOfGuest;
  }

  var apiKey = "dvxpWrF1lW3ITKs85zY3e6q7UnVE7zUD";
  //TODO Lab 2 implement the data structure that will hold number of guest
  // and selected dinner options for dinner menu
  this.guests = 1;

  this.dishType = 'meal';
  var dishOk;
  var dishOkcookie = $cookieStore.get('dishOk');
  
  this.dishObjectArray = {
      SaladObject : null,
      mainDishObject : null,
      dessertObject : null,
    };
  this.dishIdArray = {
      saladId : 0,
      mainId : 0,
      dessertId : 0,
  };

this.DishSearch = $resource('http://api.bigoven.com/recipes',{pg:1,rpp:25,api_key:'dvxpWrF1lW3ITKs85zY3e6q7UnVE7zUD'});
this.Dish = $resource('http://api.bigoven.com/recipe/:id',{api_key:'dvxpWrF1lW3ITKs85zY3e6q7UnVE7zUD'}); 

  this.updateMealCookie = function() {
    $cookieStore.put('saladId',this.dishIdArray.saladId);
    $cookieStore.put('mainId',this.dishIdArray.mainId);
    $cookieStore.put('dessertId',this.dishIdArray.dessertId);
    saladCookie = $cookieStore.get('saladId');
    mainCookie = $cookieStore.get('mainId');
    dessertCookie = $cookieStore.get('dessertId');
    console.log('cookie updated. salad: '+ saladCookie + ' main:' + mainCookie + ' dessert:' + dessertCookie);
  }
  console.log(this.dishObjectArray);

  var saladCookie = $cookieStore.get('saladId'); //cookies of the ID
  var mainCookie = $cookieStore.get('mainId');
  var dessertCookie = $cookieStore.get('dessertId');
  
  this.updateMeal = function(){
    this.dishIdArray.saladId = saladCookie;
    this.dishIdArray.mainId = mainCookie;
    this.dishIdArray.dessertId = dessertCookie;

    context = this;
    this.Dish.get({id:saladCookie},function(detail){
      context.dishObjectArray.SaladObject=detail;
    });
    this.Dish.get({id:mainCookie},function(detail){
      context.dishObjectArray.mainDishObject = detail;
    });

    this.Dish.get({id:dessertCookie},function(detail){
      context.dishObjectArray.dessertObject = detail;
    });
    console.log(this.dishObjectArray);
  }

  if((saladCookie != undefined)||(mainCookie != undefined)||(dessertCookie != undefined)){ //not the first load of the page
    console.log('not init, cookie loaded. salad:'+ saladCookie + ' main:' + mainCookie + " dessert:" + dessertCookie);
    this.updateMeal();
  }
  else{ //first load of the page, number of guest and cookie init at 2
    this.updateMealCookie();
    console.log('init: Cookie= ' + saladCookie + mainCookie + dessertCookie);
  }


  //Adds the passed dish to the menu. If the dish of that type already exists on the menu
  //it is removed from the menu and the new one added.
  //function that returns all dishes of specific type (i.e. "Salad", "main dish" or "dessert")
  //you can use the filter argument to filter out the dish by name or ingredient (use for search)
  //if you don't pass any filter all the dishes will be returned

  // Angular service needs to return an object that has all the
  // methods created in it. You can consider that this is instead
  // of calling var model = new DinnerModel() we did in the previous labs
  // This is because Angular takes care of creating it when needed.
  return this;

});
