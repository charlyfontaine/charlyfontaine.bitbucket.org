dinnerPlannerApp.controller('YourMealCtrl', function ($scope,Dinner, $cookieStore) {

	// $scope.dishDisplay = 'meal';
  // $scope.dishDisplayfunc = function(){
//Taking care of the salad cookie

  var saladIm ;
  var saladImCookie = $cookieStore.get('saladIm');
  var saladTitle;
  var saladTitleCookie = $cookieStore.get('saladTitle');
  console.log(Dinner.dishObjectArray.SaladObject + "  " + Dinner.dishIdArray.saladId);
  if((saladImCookie != undefined )&&(saladTitleCookie != undefined)&&( Dinner.dishObjectArray.SaladObject != null)){ //not the first load of the page but landing
    console.log('not init cookie load');

    saladIm = Dinner.dishObjectArray.SaladObject.ImageURL;
    $cookieStore.put('saladIm',saladIm);
    saladImCookie = $cookieStore.get('saladIm');

    saladTitle = Dinner.dishObjectArray.SaladObject.Title;
    $cookieStore.put('saladTitle',saladTitle);
    saladTitleCookie = $cookieStore.get('saladTitle');

    saladIm=saladImCookie;
    saladTitle=saladTitleCookie;
	console.log("not first landing");
  }
  else if ((saladImCookie == undefined )&&(saladTitleCookie == undefined)&&(Dinner.dishObjectArray.SaladObject != null)){ //first load of the page
    saladIm = Dinner.dishObjectArray.SaladObject.ImageURL;
    $cookieStore.put('saladIm',saladIm);
    saladImCookie = $cookieStore.get('saladIm');

    saladTitle = Dinner.dishObjectArray.SaladObject.Title;
    $cookieStore.put('saladTitle',saladTitle);
    saladTitleCookie = $cookieStore.get('saladTitle');
    console.log("first landing");
  }
  else if (Dinner.dishObjectArray.SaladObject == null){ //cookies exist and reload of page
  	saladIm=saladImCookie;
    saladTitle=saladTitleCookie;
    console.log("reload of the page");
  }

/////////////////////////////////

//Taking care of the main cookie
  var mainIm;
  var mainImCookie = $cookieStore.get('mainIm');
  var mainTitle;
  var mainTitleCookie = $cookieStore.get('mainTitle');
  
  if((mainImCookie != undefined )&&(mainTitleCookie != undefined)&&( Dinner.dishObjectArray.mainDishObject != null)){ //not the first load of the page but landing
    console.log('not init cookie load');

    mainIm = Dinner.dishObjectArray.mainDishObject.ImageURL;
    $cookieStore.put('mainIm',mainIm);
    mainImCookie = $cookieStore.get('mainIm');

    mainTitle = Dinner.dishObjectArray.mainDishObject.Title;
    $cookieStore.put('mainTitle',mainTitle);
    mainTitleCookie = $cookieStore.get('mainTitle');

    mainIm=mainImCookie;
    mainTitle=mainTitleCookie;
	console.log("not first landing");
  }
  else if ((mainImCookie == undefined )&&(mainTitleCookie == undefined)&&(Dinner.dishObjectArray.mainDishObject != null)){ //first load of the page
    mainIm = Dinner.dishObjectArray.mainDishObject.ImageURL;
    $cookieStore.put('mainIm',mainIm);
    mainImCookie = $cookieStore.get('mainIm');

    mainTitle = Dinner.dishObjectArray.mainDishObject.Title;
    $cookieStore.put('mainTitle',mainTitle);
    mainTitleCookie = $cookieStore.get('mainTitle');
    console.log("first landing");
  }
  else if (Dinner.dishObjectArray.mainDishObject == null){ //cookies exist and reload of page
  	mainIm=mainImCookie;
    mainTitle=mainTitleCookie;
    console.log("reload of the page");
  }
/////////////////////////////////

//Taking care of the dessert cookie
  var dessertIm;
  var dessertImCookie = $cookieStore.get('dessertIm');
  var dessertTitle;
  var dessertTitleCookie = $cookieStore.get('dessertTitle');
  
  if((dessertImCookie != undefined )&&(dessertTitleCookie != undefined)&&( Dinner.dishObjectArray.dessertObject != null)){ //not the first load of the page but landing
    console.log('not init cookie loaded');

    dessertIm = Dinner.dishObjectArray.dessertObject.ImageURL;
    $cookieStore.put('dessertIm',dessertIm);
    dessertImCookie = $cookieStore.get('dessertIm');

    dessertTitle = Dinner.dishObjectArray.dessertObject.Title;
    $cookieStore.put('dessertTitle',dessertTitle);
    dessertTitleCookie = $cookieStore.get('dessertTitle');

    dessertIm=dessertImCookie;
    dessertTitle=dessertTitleCookie;
	console.log("not first landing");
  }
  else if ((dessertImCookie == undefined )&&(dessertTitleCookie == undefined)&&(Dinner.dishObjectArray.dessertObject != null)){ //first load of the page
    dessertIm = Dinner.dishObjectArray.dessertObject.ImageURL;
    $cookieStore.put('dessertIm',dessertIm);
    dessertImCookie = $cookieStore.get('dessertIm');

    dessertTitle = Dinner.dishObjectArray.dessertObject.Title;
    $cookieStore.put('dessertTitle',dessertTitle);
    dessertTitleCookie = $cookieStore.get('dessertTitle');
    console.log("first landing");
  }
  else if (Dinner.dishObjectArray.dessertObject == null){ //cookies exist and reload of page
  	dessertIm=dessertImCookie;
    dessertTitle=dessertTitleCookie;
    console.log("reload of the page");
  }
/////////////////////////////////
  $scope.updatePriceTemp = function(){
        $scope.saladPrice = 0;
    
        for ( i = 0; i < Dinner.dishObjectArray.SaladObject.Ingredients.length; i++){
        	$scope.saladPrice += Dinner.dishObjectArray.SaladObject.Ingredients[i].Quantity;
        }
        $scope.mainPrice = 0;
        for ( i = 0; i < Dinner.dishObjectArray.mainDishObject.Ingredients.length; i++){
        	$scope.mainPrice += Dinner.dishObjectArray.mainDishObject.Ingredients[i].Quantity;
        }
        $scope.dessertPrice = 0;
        for ( i = 0; i < Dinner.dishObjectArray.dessertObject.Ingredients.length; i++){
        	$scope.dessertPrice += Dinner.dishObjectArray.dessertObject.Ingredients[i].Quantity;
        }
        
    	console.log($scope.mealPrice);
    }


    $scope.saladDisplay = {Title:saladTitle,ImageURL:saladIm};
    $scope.mainDisplay = {Title:mainTitle,ImageURL:mainIm};
    $scope.dessertDisplay = {Title:dessertTitle,ImageURL:dessertIm};

// Cookie for the price
  var saladPricetemp;
  var saladPriceCookie = $cookieStore.get('saladPricetemp');
  var mainPricetemp;
  var mainPriceCookie = $cookieStore.get('mainPricetemp');
  var dessertPricetemp;
  var dessertPriceCookie = $cookieStore.get('dessertPricetemp');
  
  if((saladPriceCookie != undefined )&&(mainPriceCookie != undefined )&&(dessertPriceCookie != undefined )&&( Dinner.dishObjectArray.dessertObject == null)){ //not the first load of the page
    console.log('not init cookie load');
    saladPricetemp=saladPriceCookie;
    $scope.saladPrice = saladPriceCookie;
    mainPricetemp=mainPriceCookie;
    $scope.mainPrice = mainPriceCookie;
    dessertPricetemp=dessertPriceCookie;
    $scope.dessertPrice = dessertPriceCookie;
    $scope.mealPrice = ($scope.saladPrice + $scope.mainPrice + $scope.dessertPrice)*Dinner.getNumberOfGuests();
  }
  else if ((dessertImCookie != undefined )&&( Dinner.dishObjectArray.dessertObject != null)){//Reload 
  	$scope.updatePriceTemp();
  	$scope.mealPrice = ($scope.saladPrice + $scope.mainPrice + $scope.dessertPrice)*Dinner.getNumberOfGuests();
  		saladPricetemp = $scope.saladPrice;
  		mainPricetemp = $scope.mainPrice;
  		dessertPricetemp = $scope.dessertPrice;

    $cookieStore.put('saladPricetemp',saladPricetemp);
    saladPriceCookie = $cookieStore.get('saladPricetemp');
        $cookieStore.put('mainPricetemp',mainPricetemp);
    mainPriceCookie = $cookieStore.get('mainPricetemp');
        $cookieStore.put('dessertPricetemp',dessertPricetemp);
    dessertPriceCookie = $cookieStore.get('dessertPricetemp');
  }
  else if((saladPriceCookie == undefined )&&(mainPriceCookie == undefined )&&(dessertPriceCookie == undefined )){ //first load of the page
   $scope.updatePriceTemp();
   $scope.mealPrice = ($scope.saladPrice + $scope.mainPrice + $scope.dessertPrice)*Dinner.getNumberOfGuests();
  		saladPricetemp = $scope.saladPrice;
  		mainPricetemp = $scope.mainPrice;
  		dessertPricetemp = $scope.dessertPrice;

    $cookieStore.put('saladPricetemp',saladPricetemp);
    saladPriceCookie = $cookieStore.get('saladPricetemp');
        $cookieStore.put('mainPricetemp',mainPricetemp);
    mainPriceCookie = $cookieStore.get('mainPricetemp');
        $cookieStore.put('dessertPricetemp',dessertPricetemp);
    dessertPriceCookie = $cookieStore.get('dessertPricetemp');
  }

/////////////////////////////////
	$scope.updatePrice= function(){
		$scope.mealPrice = ($scope.saladPrice + $scope.mainPrice + $scope.dessertPrice)*Dinner.getNumberOfGuests();
		console.log($scope.mealPrice);
	}
  // }
});