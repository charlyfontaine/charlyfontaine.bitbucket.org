
//----------------Parralax-------------
var controllerPrlx = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: "onEnter", duration: "200%"}});

    // build scenes
    new ScrollMagic.Scene({triggerElement: "#parallax1"})
                    .setTween("#parallax1 > div", {y: "80%", ease: Linear.easeNone})
                    .addIndicators()
                    .addTo(controllerPrlx);

    new ScrollMagic.Scene({triggerElement: "#parallax2"})
                    .setTween("#parallax2 > div", {y: "80%", ease: Linear.easeNone})
                    .addIndicators()
                    .addTo(controllerPrlx);

    new ScrollMagic.Scene({triggerElement: "#parallax3"})
                    .setTween("#parallax3 > div", {y: "80%", ease: Linear.easeNone})
                    .addIndicators()
                    .addTo(controllerPrlx);
    new ScrollMagic.Scene({triggerElement: "#parallax5"})
                    .setTween("#parallax5 > div", {y: "80%", ease: Linear.easeNone})
                    .addIndicators()
                    .addTo(controllerPrlx);

//---------------------------------------
$('.html_popup').popup({
  content : '<h1>This is some HTML</h1>',
  type : 'html'
});
//----------------SVG Animation-------------
    function pathPrepare ($el) {
        var lineLength = $el[0].getTotalLength();
        $el.css("stroke-dasharray", lineLength);
        $el.css("stroke-dashoffset", lineLength);
    }

    var $word = $("path#word");
    var $dot = $("path#dot");

    // prepare SVG
    pathPrepare($word);
    pathPrepare($dot);

    // init controller
    var controllerSVG = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: "onLeave", offset:-250}});

    // build tween
    var tweenSVG = new TimelineMax()
        .add(TweenMax.to($word, 0.9, {strokeDashoffset: 0, ease:Linear.easeNone})) // draw word for 0.9
        .add(TweenMax.to($dot, 0.1, {strokeDashoffset: 0, ease:Linear.easeNone}))  // draw dot for 0.1
        .add(TweenMax.to("path", 1, {stroke: "#33629c", ease:Linear.easeNone}), 0);        // change color during the whole thing


    // build scene
    var scene = new ScrollMagic.Scene({triggerElement: "#trigger1", duration: 200, tweenChanges: true})
                    .setTween(tweenSVG)
                    .addIndicators() // add indicators (requires plugin)
                    .addTo(controllerSVG);
//-------------------------------------------------

//----------------Profile pic Animation-------------
var controllerImg = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: "onLeave", offset:20}});
TweenMax.set(".circle-image");
var tweenImg = new TimelineMax()
    .to(".circle-image", 0.1, {top: "15%", left:"-10%",scale: 0.5, position:"fixed"}
        );

var sceneImg = new ScrollMagic.Scene({triggerElement: "#scale-animation"})
                    .setTween(tweenImg)
                    .addIndicators({name: "timeline"}) // add indicators (requires plugin)
                    .addTo(controllerImg);
//------------------------------------------------
//----------------World Map-------------
var map = new Datamap({
        scope: 'world',
        element: document.getElementById('container1'),
        projection: 'mercator',
        height: 500,
        fills: {
          defaultFill: '#f0af0a',
          lt50: 'rgba(0,244,244,0.9)',
          gt50: 'red'
        },
        
        data: {
          USA: {fillKey: 'lt50' },
          RUS: {fillKey: 'lt50' },
          CAN: {fillKey: 'lt50' },
          BRA: {fillKey: 'gt50' },
          ARG: {fillKey: 'gt50'},
          COL: {fillKey: 'gt50' },
          AUS: {fillKey: 'gt50' },
          ZAF: {fillKey: 'gt50' },
          MAD: {fillKey: 'gt50' }       
        }
      })
      
      
      //sample of the arc plugin
      map.arc([
       {
        origin: {
            latitude: 40.639722,
            longitude: 73.778889
        },
        destination: {
            latitude: 37.618889,
            longitude: -122.375
        }
      },
      {
          origin: {
              latitude: 30.194444,
              longitude: -97.67
          },
          destination: {
              latitude: 25.793333,
              longitude: -0.290556
          }
      }
      ], {strokeWidth: 2});
       
      
       //bubbles, custom popup on hover template
     map.bubbles([
       {name: 'Hot', latitude: 21.32, longitude: 5.32, radius: 10, fillKey: 'gt50'},
       {name: 'Chilly', latitude: -25.32, longitude: 120.32, radius: 18, fillKey: 'lt50'},
       {name: 'Hot again', latitude: 21.32, longitude: -84.32, radius: 8, fillKey: 'gt50'},

     ], {
       popupTemplate: function(geo, data) {
         return "<div class='hoverinfo'>It is " + data.name + "</div>";
       }
     });
//------------------------------------------------