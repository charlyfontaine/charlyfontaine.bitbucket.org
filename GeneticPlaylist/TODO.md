Genetic Playlist //TODO:
========================


## Simulation & Paper.js

* visualize Individual Tracks =>
	- round pictures =>

* visualize Fitness => 
	- first iteration =>

* visualize Selection => 
	- first iteration => check
	- flicker when dying bug =>

* visualize Crossover
	- first iteration => check
	- pick better mating target position =>
	- 

* handle unavailable songs => 
* restart OR continue simulation with new track(s) =>
* subscribe to songChange broadcast from playback (highlight currently playing) =>
* automatic 'click' on the Generation Button =>
* 


## GEN
* provide Options for the GEN Service, which can be queried.
	- example: 	GEN.maxFitness();
				GEN.mutationProbability();


* if the imported playlist has too many songs, kill until the max is reached


* Make the selection more intelligent, i.e. dynamic threshold ?

* cross-over gives a random song from a random album of a similar artist to each pair of alive songs 
* make selection take care of the number of killed songs so more than X% survive


## Front End
* Show playlist folders => (TOO HAARD DONT WANT TOooo) #LENNY
* Show playlist tracks when clicking playlist => CHECK
* Drag-n-drop playlists or tracks to main view 	=> (Too little time click works for now)


## General
* Play song in spotify => CHECK
* Export playlist to/and open spotify => CHECK


## Bugs
* crossover alomst never kills anyone =>
* startup bug: cant yield songs sometimes =>
* Game::message does not update when window not in focus =>

* Boid no longer inside of canvas after resize => 
* Borders not working => 

#DONE
* No subpixel accuracy on css boid position => CHECK
* Selection not working at all anymore => CHECK
* GET for album.jpg yields error => CHECK
* Hover Pause Button not working => CHECK
* Boids on top of hover div => CHECK
* scroll not functional on aboutus page => DONE
* calculate the fitness according to the song's value from the API => first iteration check
* explore 'directive' solution 	=> ABANDON?
* remove track from simulation => CHECK
* Fix canvas lower border (it is under the lowest pane atm)
* fitness basic function => CHECK
* selection with a threshold => CHECK
* getPlaylistTracks promise bugs out for some playlists => CHECK (hotfix, catch gives empty playlisttracks)
* restart OR continue simulation with new playlist => CHECK
* add songs to simulation without rerouting => CHECK
* design GEN API =>	CHECK
* add default sprite for non-existing album/artist art => CHECK
* Hello World => CHECK
* Click a NextGeneration Button to call a Selection procedure => CHECK
* Click song for preview => CHECK
* Create some shape for each song in routed playlist => CHECK
* Setup Paper.js in mainview => CHECK
* change ng-view to non-scrollable somehow => CHECK
* Update app when not in focus => CHECK for canvas
* Create a 'genetics service', probably similar to how the API functions work => CHECK
