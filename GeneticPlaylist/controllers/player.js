(function() {

	var module = angular.module('PlayerApp');

	module.controller('PlayerController', function($scope, $rootScope, Auth, API, PlayQueue, Playback, $location) {
		$scope.view = 'welcome';
		$scope.username = Auth.getUsername();
		$scope.playlists = [];
		$scope.playing = false;
		$scope.progress = 0;
		$scope.duration = 4000;
		$scope.trackdata = null;
		$scope.currenttrack = '';
		$scope.tracks = [];

		function updatePlaylists() {
			if ($scope.username != '') {
				API.getPlaylists(Auth.getUsername()).then(function(items) {

					items = items.filter(function(pl){
						return $scope.username === pl.owner.id;
					});

					$scope.playlists = items.map(function(pl) {
						return {
							id: pl.id,
							name: pl.name,
							uri: pl.uri,
							username: pl.owner.id,
							collaborative: pl.collaborative,
							'public': pl['public'],

						};
					});
					
				});
			}
			
		}

		updatePlaylists();

		// subscribe to an event
		$rootScope.$on('playlistsubscriptionchange', function() {
			updatePlaylists();
		});

		var currentList="";

		$scope.toggleSongs = function(playlistId){
			//console.log("Songs toggled.");
			if(currentList == playlistId){
				currentList = "";
				$("#"+playlistId).slideUp('slow');
				$(".songs").empty();
			}
			else{
				currentList = playlistId;
				$(".songs").empty();
			
				API.getPlaylistTracks($scope.username,playlistId).then(function(tracks) {
					//console.log('got user tracks', tracks);
					
					$("#"+playlistId).hide();
					for (x in tracks.items){
						////console.log(tracks.items[x].track.name);
						$("#"+playlistId).append("<li><div id='"+tracks.items[x].track.id+"' class='songName'>"+tracks.items[x].track.name+"</div></li>");
					}
					$("#"+playlistId).slideDown('slow');
				});
				}
			}
				
		$(document).on('click','div.songName',function(event){
			////console.log($(this).attr('id'));
			$scope.addSong($(this).attr('id'));
		});

		
		$(document).on('click','div.genSongName',function(event){
			////console.log($(this).attr('id'));
			pId = $(this).attr('id').substring(3);
			////console.log(pId);
			window.location.href = 'spotify:track:'+pId;
		});
    
		$scope.tempRestart = function() {
		   $scope.$broadcast('restart');
		}; 

		$scope.addSongs = function(playlistId) {
		   $scope.$broadcast('addPlaylist', playlistId);
		};    

		$scope.addSong = function(songId) {
			////console.log(songId);
			API.getTrack(songId).then(function(song){
				$scope.$broadcast('addSong', song);
			});
		};

		$scope.tempStartGeneration = function(){
			////console.log("test1");
			$scope.$broadcast('startGeneration');
		};

		$scope.tempExportPlaylist = function(){
			////console.log("test2");
			$scope.$broadcast('exportPlaylist');
		};


		$scope.logout = function() {
			//console.log('do logout...');
			Auth.setUsername('');
			Auth.setAccessToken('', 0);
			$scope.$emit('logout');
		};

		$scope.about = function() {
			//console.log('do about...');
			$scope.$emit('about');
		};		

		$scope.resume = function() {
			Playback.resume();
		};

		$scope.pause = function() {
			Playback.pause();
		};

		$scope.next = function() {
			PlayQueue.next();
			Playback.startPlaying(PlayQueue.getCurrent());
		};

		$scope.prev = function() {
			PlayQueue.prev();
			Playback.startPlaying(PlayQueue.getCurrent());
		};

		$scope.queue = function(trackuri) {
			PlayQueue.enqueue(trackuri);
		};

		$scope.showhome = function() {
			//console.log('load home view');
		};

		$scope.showplayqueue = function() {
			//console.log('load playqueue view');
		};

		$scope.showplaylist = function(playlisturi) {
			//console.log('load playlist view', playlisturi);
		};

		$scope.query = '';

		$scope.loadsearch = function() {
			//console.log('search for', $scope.query);
			$location.path('/search').search({ q: $scope.query }).replace();
		};


		$scope.volume = Playback.getVolume();

		$scope.changevolume = function() {
			Playback.setVolume($scope.volume);
		};

		$scope.changeprogress = function() {
			Playback.setProgress($scope.progress);
		};

		$rootScope.$on('login', function() {
			//console.log("on login..");
			$scope.username = Auth.getUsername();
			updatePlaylists();
		});

		$rootScope.$on('playqueuechanged', function() {
			//console.log('PlayerController: play queue changed.');
			// $scope.duration = Playback.getDuration();
		});

		$rootScope.$on('playerchanged', function() {
			//console.log('PlayerController: player changed.');
			$scope.currenttrack = Playback.getTrack();
			$scope.playing = Playback.isPlaying();
			$scope.trackdata = Playback.getTrackData();
		});

		$rootScope.$on('endtrack', function() {
			//console.log('PlayerController: end track.');
			$scope.currenttrack = Playback.getTrack();
			$scope.trackdata = Playback.getTrackData();
			$scope.playing = Playback.isPlaying();
			PlayQueue.next();
			Playback.startPlaying(PlayQueue.getCurrent());
			$scope.duration = Playback.getDuration();
		});

		$rootScope.$on('trackprogress', function() {
			//console.log('PlayerController: trackprogress.');
			$scope.progress = Playback.getProgress();
			$scope.duration = Playback.getDuration();
		});


	});

})();
