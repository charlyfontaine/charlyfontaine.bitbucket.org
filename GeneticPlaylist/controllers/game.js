  (function() {
 
  var module = angular.module('PlayerApp');

  module.controller('GameController', function($scope, $rootScope, $q, GEN, API, PlayQueue, $routeParams, Auth) {
            $scope.On = "none";
            var n = 20, // number of layers
                m = 13; // number of samples per layer
            stack = d3.layout.stack().offset("wiggle");

            var width = document.getElementById("mainview").offsetWidth,
              height = 500;
            var svgContainer = d3.select("#profile").append("svg")
              .attr("width", width)
              .attr("height", height);

            var svg = svgContainer.append("svg");
            var svg1 = svgContainer.append("svg");
            function cleanSVG(){
            
            d3.select("svg").remove();

             svgContainer = d3.select("#profile").append("svg")
              .attr("width", width)
              .attr("height", height);

             svg = svgContainer.append("svg");
             svg1 = svgContainer.append("svg");
 
            }
            function background(data){
              cleanSVG();
              $scope.On = "inline"
             // console.log(la);
             //console.log(bumpLayer(m));
              layers0=data;//stack(d3.range(n).map(function() {return la;}));
              //console.log(d3.range(n).map(function() {return bumpLayer(m); }));
            var x = d3.scale.linear()
                .domain([0, m - 1])
                .range([0, width]);

            var y = d3.scale.linear()
                .domain([0, d3.max(layers0.concat(layers0), function(layer) { return d3.max(layer, function(d) { return d.y0 + d.y; }); })])
                .range([height, 0]);

            var color = d3.scale.linear()
              .range(["blue", "purple"]);

            var area = d3.svg.area()
                .interpolate("cardinal")
                .y0(function(d) { return y(d.y0); })
                .x(function(d) { return x(d.x);})
                .y1(function(d) { return y(d.y0 + d.y);});
        

          // console.log(svgContainer);
          svg.selectAll("path")
              .data(layers0)
              .enter().append("path")
              .attr("d", area)
              .transition()
              .duration(2500)
              .style("fill", function() { return color(Math.random()); });
        }
        
        function transitionProfile(data, fitnessColor) {
            console.log("transition");
            layers1 = data;
            //stack(d3.range(n).map(function() {return bumpLayer(m); }));
          var x = d3.scale.linear()
              .domain([0, m - 1])
              .range([0, width]);
         
          var y = d3.scale.linear()
              .domain([0, d3.max(layers0.concat(layers1), function(layer) { return d3.max(layer, function(d) { return d.y0 + d.y; }); })])
              .range([height, 0]);
          
          var area = d3.svg.area()
              .interpolate("cardinal")
              .y0(function(d) { return y(d.y0); })
              .x(function(d) { return x(d.x);})
              .y1(function(d) { return y(d.y0 + d.y);});
          
          var color = d3.scale.linear()
              .range(["white", fitnessColor])
              .interpolate(d3.interpolateLab);

            svg1.selectAll("path")
              .data(layers1)
              .enter().append("path")
              .attr("d", area)
              .transition()
              .duration(2500)
              .style("fill", fitnessColor)
              .remove();

           }

      
          function bumpLayer(n) {

            function bump(a) {
              var x = 1 / (.1 + Math.random()),
                  y = 2 * Math.random() - .5,
                  z = 10 / (.1 + Math.random());
              for (var i = 0; i < n; i++) {
                var w = (i / n - y) * z;
                a[i] += x * Math.exp(-w * w);
              }
            }

            var a = [], i;
            for (i = 0; i < n; ++i) a[i] = 0;
            for (i = 0; i < 5; ++i) bump(a);
            return a.map(function(d, i) { return {x: i, y: Math.max(0, d)}; });
          }
    console.log("in Game Controller: ");

    var promiseStack = []; // promises before initializing game
    $scope.username = Auth.getUsername();
    //$scope.playlistId = $routeParams.playlist;
    //if($scope.playlistId == undefined) {
    //  $scope.message = "Choose a seed playlist!";
    //  return;
    //}
    /* Game Variables */
    $scope.initializingGame = true;
    $scope.runningGame = false;

    $scope.boidIdCounter = 0;

    $scope.boidOptions = {
      maxSpeed: 100.0,
      minSpeed: 20.0,
      maxForce: 0.3,
      boundsForceFactor: 2.0,
      spriteScale: 0.04,
      tracknameOffset: new paper.Point(0, -25),
      arriveDistance: 20.0,
      separateForceFactor: 0.1,
      alignForceFactor: 0.05,
      coheseForceFactor: 0.1,
      separationRadius: 50.0,
      alignmentRadius: 40.0,
      trotRadius: 40.0,
      trotProbPerFrame: 0.005
    };

    $scope.algvizOptions = {
      fitnessVistime:   100, //ms
      selectionVistime: 200, //ms
      crossoverVistime: 1500, //ms
    };
    
    var AlgVis =  { // use like enum
                    none: 0,
                    fitness: 1,
                    selection: 2,
                    crossover: 3
                  };
    $scope.currentAlgVis = AlgVis.none;

    var visData = {
      fitness:    {}, //TODO: attach to boids instead?
      selection:  {}, 
      crossovers: {},
    }

    $scope.boids = [];

    /* Math Utilities */

    function lerp(a, b, t) {
      return a + (b-a)*t;
    };

    function clampVector(vec, min, max) {

      var res = vec.clone();
      if(vec.length > max) {
        res = res.normalize();
        res = res.multiply(max);
      }
      if(vec.length < min) {
        res = res.normalize();
        res = res.multiply(min); 
      }

      return res;
    };

    /* Game Functions */
    function addBoid(track, position) {
      position = position || paper.Point.random().multiply(paper.view.size);
      var newBoid = new Boid( $scope.boidIdCounter,
                                  track, 
                                  position, 
                                  $scope.boidOptions.maxSpeed,
                                  $scope.boidOptions.minSpeed,
                                  $scope.boidOptions.maxForce);

      $scope.boids.push(newBoid);      
      $scope.boidIdCounter++;
      //console.log($scope.boids);
      $scope.$broadcast('changeBoids');
      return newBoid;
    }


    /* Game Classes */

    var Boid = paper.Base.extend({
      initialize: function(id, track, position, maxSpeed, minSpeed, maxForce, profile) {
        this.id = id;
        this.track = track;
        this.track.boidId = this.id; //needed for the genetic generation process
        // console.log('boid: ' + this.id);
        this.timerAccum = 0.0;

        var strength = Math.random() * 0.5;
        this.acceleration = new paper.Point();
        
        this.velocity = paper.Point.random().multiply(maxSpeed*2);
        this.velocity = this.velocity.subtract(maxSpeed);
        
        this.position = position.clone();

        this.radius = 30;
        this.maxSpeed = maxSpeed + strength;
        this.minSpeed = minSpeed;
        this.maxForce = maxForce + strength;
        this.minForce = 0.0;
        this.lastMaxSpeed = 0;
        this.lastVelocity = 0;

        this.focusTarget = false;
        this.currentTarget = null;

        this.family = {
          siblings: [],
          parents: [],
          children: []
        };

        this.deferredCrossover = null;
        this.crossover = null;
        this.responsibleParent = false;
        this.clearCrossover();
        this.isPlaying = false;
        this.height = 30;
        this.width  = 30;
        this.createGraphics();
      },

      createGraphics: function() {
        
        this.spriteUrl = getArtUrl(this.track);

        //API.getTrackProfile(this.track.id).then(function(result) { //CHECK THE GEN SINCE WE HAVE A RATE LIMIT ON ECHONEST
        //  // TODO: deal with visualzing the track profile 
        //  console.log(result);  
        //});        

        var boidImg = $('<img />');
            boidImg.attr("src", this.spriteUrl);
            boidImg.css("width", this.width + "px");
            boidImg.css("height", this.height + "px");
            //boidImg.css("mask", "url(images/path.svg) top left / cover");
            boidImg.attr("class", "fish");

        var boidContainer =  $("<div />");
            boidContainer.css("position", "absolute");
            boidContainer.css("top", "0");
            boidContainer.css("left", "0");
            //boidContainer.css("transition", 'all 0.1s ease-out');

            moveElement(boidContainer, this.position.x, this.position.y);
            boidContainer.attr("id", "boid_" + this.id);
            boidContainer.attr("class", "boidCtnr");
            boidContainer.append(boidImg);

        var infoContainer = $( "#trackInfoContainerSample").clone(); 
            infoContainer.attr("id", "");
            boidContainer.append(infoContainer);
            $('.mainview').append(boidContainer);
        
        var boidProfile=[{x:0,y:0},{x:1,y:0},{x:2,y:0},{x:3,y:0},{x:4,y:0},{x:5,y:0},{x:6,y:0},{x:7,y:0},{x:8,y:0},
                        {x:9,y:0},{x:10,y:0},{x:11,y:0},{x:12,y:0}];
        var test = this.track;
        //this.debugDot = new Path.Circle(this.position, 3.0);
        //this.debugDot.fillColor = 'white';
        //this.debugDot.opacity = 0.6;

        var trackboid = this;

        boidContainer.find("#trackPlayBtn").click(function() {

          var trackuri = trackboid.track.uri;
          if(!this.isPlaying){
                      boidContainer.find("#trackPlayBtn").css("background-image", 'url("images/btn-pause.png")');
                      this.isPlaying = true;
                      PlayQueue.clear();
                      PlayQueue.enqueue(trackuri);
                      PlayQueue.playFrom(0);
          }else
          {
                      boidContainer.find("#trackPlayBtn").css("background-image", 'url("images/btn-play.png")');
                      this.isPlaying = false;
                      $scope.pause();
                      PlayQueue.clear();
          }

        });

        boidContainer.mouseover(function()  {
        
          if (test.profile !=undefined){
          //$scope.PFL = "Fitness:" + test.fitness.toFixed(2) + "/10";  
          boidProfile[0].y = test.profile.acousticness;
          boidProfile[1].y = test.profile.danceability;
          boidProfile[2].y = test.profile.duration;
          boidProfile[3].y = test.profile.energy;
          boidProfile[4].y = test.profile.instrumentalness;
          boidProfile[5].y = test.profile.key;
          boidProfile[6].y = test.profile.liveness;
          boidProfile[7].y = test.profile.loudness;
          boidProfile[8].y = test.profile.mode;
          boidProfile[9].y = test.profile.speechiness;
          boidProfile[10].y= test.profile.tempo;
          boidProfile[11].y= test.profile.time_signature;
          boidProfile[12].y= test.profile.valence;
          test.profil =  stack(d3.range(n).map(function() {return boidProfile;}));
            transitionProfile(test.profil, getColorFromValue(trackboid.fitness));
          }
          
          
          // $scope.PFL = boid.profile;
          moveElement(boidContainer, truncate(trackboid.position.x), truncate(trackboid.position.y));

          boidContainer.find(".fish").hide();
          boidContainer.find(".infoCnt").show();
          boidContainer.css("z-index", "1000");

          boidContainer.find(".infoCnt").css("border-left", "7px solid " + getColorFromValue(trackboid.fitness));

          trackboid.lastMaxSpeed = trackboid.maxSpeed;
          trackboid.lastVelocity = trackboid.velocity;
          trackboid.maxSpeed = 0;

          var posX = trackboid.position.x;
          var posY = trackboid.position.y;
          var imgUrl = trackboid.spriteUrl;

          var albumName =  trackboid.track.album.name;
          var trackName =  trackboid.track.name
          var artistName = trackboid.track.artists[0].name;

          albumName = albumName.length > 15
                         ? albumName.substring(0,15) + "..."
                         : albumName.substring(0,15);

          artistName = artistName.length > 15
                         ? artistName.substring(0,15) + "..."
                         : artistName.substring(0,15);

          trackName = trackName.length > 30
                         ? trackName.substring(0,30) + "..."
                         : trackName.substring(0,30);                         

          imgUrl =  boidContainer.find(".fish").attr("src");
          boidContainer.find("#trackInfoImg").css("background-image", 'url("' + imgUrl + '")')
          boidContainer.find("#infoArtist").html(artistName);
          boidContainer.find("#infoTrackName").html(trackName);
          boidContainer.find("#infoAlbum").html(albumName);
          boidContainer.find("#infoYear").html("unknown");

          $(".boidCtnr").css('opacity', 0.2);
          boidContainer.css('opacity', 1);

          trackboid.family.parents.forEach(function(parent) {
          	$(".boidCtnr#boid_" + parent).css('opacity', 1);
          	$(".boidCtnr#boid_" + parent + ' .fish').css('transform', 'scale(1.5)');
          });

		  trackboid.family.children.forEach(function(child) {
          	$(".boidCtnr#boid_" + child).css('opacity', 1);
          	$(".boidCtnr#boid_" + child + ' .fish').css('transform', 'scale(1.5)');
          	//$(".boidCtnr#boid_" + child).css('border', '5px solid pink');
          });
        });

        boidContainer.mouseout(function(){
          boidContainer.css("z-index", "300");
          boidContainer.find(".infoCnt").hide();
          boidContainer.find(".fish").show(); 
          boidContainer.find("#trackPlayBtn").css("background-image", 'url("images/btn-play.png")');

          boidContainer.find(".infoCnt").css("border-left", "0px");

          trackboid.maxSpeed = trackboid.lastMaxSpeed;
          trackboid.velocity = trackboid.lastVelocity;

    	   $(".boidCtnr").css('opacity', 1);
    	   $(".boidCtnr .fish").css('transform', 'scale(1)');
        });

      },

      run: function(dt, boids) {
        this.borders();

        if($scope.currentAlgVis === AlgVis.fitness)   this.visualizeFitness(dt, boids);
        if($scope.currentAlgVis === AlgVis.selection) this.visualizeSelection(dt, boids);
        if($scope.currentAlgVis === AlgVis.crossover) this.visualizeCrossover(dt, boids);

        this.updateTimers(dt);
        this.flock(boids);
        this.trot();
        this.applyForce(this.seekTarget(this.currentTarget));

        this.update(dt);
      },

      updateTimers: function(dt) {
        this.timerAccum += dt*1000; //count ms
      },

      initializeFitness: function() {

      },
      
      visualizeFitness: function(dt, boids) {

        boids.forEach(function(boid) {
          var boidCont = $("#boid_" + boid.id + ' .fish');
          var trackFitness = truncate(boid.fitness);
          //console.log("Boid : " + boid.id + "  Fitness:" + boid.fitness);
          boidCont.css('border-color', getColorFromValue(trackFitness));
        });
        //-webkit-mask-box-image: url("images/start.svg");
      },

      initializeSelection: function() {
        this.timerAccum = 0.0; //use timer for dying
      },

      visualizeSelection: function(dt, boids) {
        //lerp the opacity towards 0, during the time we have

        var t_dying = this.timerAccum/$scope.algvizOptions.selectionVistime;
        //console.log(this.timerAccum);
        //console.log(t_dying);
       // this.sprite.opacity = lerp(1, 0, t_dying);
      },

      /*
        Each parent is asked to seek a target position.
        Only >one< parent per parentgroup is responsible for the spawning.
      */
      initializeCrossover: function(crossover, target, responsible) {
        console.log("initializing crossover");
        console.log(this.id);
        console.log("responsible ", responsible);
        this.deferredCrossover = $q.defer();
        this.crossover = crossover;
        this.responsibleParent = responsible;
        this.timerAccum = 0.0; //use timer for dying

        //get to target
        this.setTarget(target);

        if(!responsible) {
          //console.log("parent not responsible, resolve()");
          this.deferredCrossover.resolve();
        }

        return this.deferredCrossover.promise;
      },

      clearCrossover: function() {
        this.deferredCrossover = null;
        this.crossover = null;
        this.responsibleParent = false;
      },

      visualizeCrossover: function(dt, boids) {
        var t_crossover = this.timerAccum/$scope.algvizOptions.crossoverVistime;
        //console.log(t_crossover);
        if(t_crossover > 0.8) {
          //spawn children and resolve spawnpromise
          if(this.responsibleParent) {
            //console.log("spawning children");
            //console.log(this.crossover.children);

            var color ='#'+Math.random().toString(16).substr(2,6);

            //for (var i = 0; i < this.crossover.parents.length; i++) {
              //var parentBoidId = this.crossover.parents[i].boidId;
              // var fish = $(".mainview #boid_" + parentBoidId + " .fish");
              // fish.css("border" , "2px solid " + color);

            //};

            for (var i = 0; i < this.crossover.children.length; i++) {
              var spawnposition = this.position.add(paper.Point.random().multiply(30, 30).subtract(15,15));
              var newBoid = addBoid(this.crossover.children[i], spawnposition);
              // var fish = $(".mainview #boid_" + newBoid.id + " .fish");
              // fish.css("border" , "2px solid " + color);
            };

            this.deferredCrossover.resolve();
            this.clearCrossover();
          }
        }

      },

      update: function(dt) {
        this.velocity = this.velocity.add(this.acceleration);
        this.velocity = clampVector(this.velocity, this.minSpeed, this.maxSpeed);
        this.position = this.position.add(this.velocity.multiply(dt));

        this.position.x  = (this.position.x > $('#game-canvas').width())
                                        ? $('#game-canvas').width() - 30
                                        : this.position.x ;

        this.position.y  = (this.position.y > $('#game-canvas').height())
                                        ? $('#game-canvas').height() - 30
                                        : this.position.y ;

        this.position.x  = (this.position.x < 0)
                                        ? 0
                                        : this.position.x ;

        this.position.y  = (this.position.y < 0)
                                        ? 0
                                        : this.position.y ;


        this.acceleration = new paper.Point();
      },

      draw: function() {
        //TODO: generalize the graphics (one draw draws whole hierarchy)
        //this.sprite.position = this.position;

        var boidCont = $(".mainview #boid_" + this.id);
        moveElement(boidCont, this.position.x, this.position.y);
        //this.debugDot.position = this.currentTarget;


      },

      applyForce: function(force, maxforce) {
        maxforce = maxforce != null ? this.maxForce : maxforce;
        force = clampVector(force, this.minForce, maxforce);
        this.acceleration = this.acceleration.add(force);
      },

      setTarget: function(target) {
        this.focusTarget = true;
        this.currentTarget = target;
      },

      clearTarget: function(){
        this.focusTarget = false;
        this.currentTarget = null;
      },

      flock: function(boids) {

        var separationForce = new paper.Point(0.0,0.0);
        var alignmentForce = new paper.Point(0.0,0.0);
        var cohesionForce = new paper.Point(0.0,0.0);

        var alignmentSum = new paper.Point(0.0,0.0);
        var cohesionSum = new paper.Point(0.0,0.0);

        var algcount = 0; var cohcount = 0;
        for (var i = 0; i < boids.length; i++) {
          var boid = boids[i];
          var distance = boid.position.getDistance(this.position);
          if(distance > 0)
          {            
            //separation
            var separationRadius = $scope.boidOptions.separationRadius;
            if(distance < separationRadius) {
              var force = this.position.subtract(boid.position);
              force = force.multiply(((separationRadius - distance) / distance));
              force = force.multiply($scope.boidOptions.separateForceFactor);
              separationForce = separationForce.add(force);
            }

            //alignment
            if(distance < $scope.boidOptions.alignmentRadius) {
              alignmentForce = alignmentForce.add(boid.velocity);
              alignmentSum = alignmentSum.add(boid.velocity);
              algcount++;
            }

            //cohesion
            if(distance < $scope.boidOptions.cohesionRadius) {
              cohesionSum = cohesionForce.add(boid.position);
              cohcount++;
            }
          }

        }

        if(algcount > 0) {
          alignmentSum = alignmentForce.divide(algcount);
          alignmentForce = alignmentSum.subtract(this.velocity);
          alignmentForce = alignmentForce.multiply($scope.boidOptions.alignForceFactor);
        }

        if(cohcount > 0) {
          cohesionSum = cohesionSum.divide(cohcount);
          cohesionForce = seekTarget(cohesionSum);
        }

        //console.log(separationForce);
        this.applyForce(separationForce);
        this.applyForce(alignmentForce);
        this.applyForce(cohesionForce);

      },

      trot: function() {
        // once in a while picks a random target point
        // and applies some random movements.
        if(this.focusTarget) return;

        if(Math.random() < $scope.boidOptions.trotProbPerFrame) 
        {

          //console.log("new random trot target");
          var trot = new paper.Point(Math.random() - 0.5, Math.random() - 0.5);
          trot = trot.normalize();
          trot = trot.multiply($scope.boidOptions.trotRadius);
          var target = this.position.add(trot);
          this.currentTarget = target;

          // var raster = new Raster('mona');
          // // Move the raster to the center of the view
          // raster.position = target;

        }

        //sometimes clear target
        //if(Math.random() < $scope.boidOptions.trotProbPerFrame/2) {
        //  this.currentTarget = null;
        //}


      },

      seekTarget: function(target) {
        if(this.currentTarget === null) return new paper.Point(0,0);
        var desired = target.subtract(this.position);
        var dist = desired.length;
        desired = desired.normalize();

        //arriving behaviour
        if(dist < $scope.boidOptions.arriveDistance) {
          var m = dist / $scope.boidOptions.arriveDistance;
          desired = desired.multiply(m);
        } else {
          desired = desired.multiply(this.maxSpeed);
        }

        var steer = desired.subtract(this.velocity);
        steer = clampVector(steer, this.minForce, this.maxForce);

        return steer;
      },

      borders: function() {

        var margin = new paper.Point(30, 30);
        var force = new paper.Point();
        var centerToPos = this.position.subtract(paper.view.center);
        var minDiff = centerToPos.add(paper.view.size.multiply(0.5));
        var maxDiff = centerToPos.subtract(paper.view.size.multiply(0.5));

        minDiff = minDiff.subtract(margin)
        maxDiff = maxDiff.add(margin);

        var friction = 0.0;

        var coord = ["x", "y"];
        for (var i = 0; i < 2; ++i)
        {
            if (minDiff[coord[i]] < 0)
                force[coord[i]] = minDiff[coord[i]];
            else if (maxDiff[coord[i]] > 0)
                force[coord[i]] = maxDiff[coord[i]];
            else
                force[coord[i]] = 0;

            friction += Math.abs(force[coord[i]]);
        }

        force = force.add(this.velocity.multiply(0.1 * friction));
        force = force.multiply(-$scope.boidOptions.boundsForceFactor);
        
        this.applyForce(force);
      },


      clear: function() {
       // this.sprite.remove();
        //this.debugDot.remove();
        var MyCover = $(".mainview #boid_" + this.id);
        MyCover.fadeOutAndRemove(4000);
        $("#trackInfoContainer").hide();
      },

    });


    /* Game Functions */

    function initGame() {
      console.log("INITIALIZING GAME");
      $scope.runningGame = true;
    }

    function getArtUrl(track) {
      if(track.album.images[0] == undefined)
        return "images/placeholder-playlist.png";
      return track.album.images[0].url;
    }

    function moveElement(elem, posX, posY) {

       // elem.css({ 'left': posX +'px', 'top': posY+'px' });
       // return;
      var translation = 'translate3d('+posX+'px,'+posY+'px, 0px)';
      if (Modernizr.csstransforms && Modernizr.csstransitions) {
        elem.css({ 
                  '-webkit-transform': translation,
                  '-ms-transform': translation,
                  'transform3d':translation,
                  '-webkit-transition': '-webkit-transform3d 16ms linear'
               }); 
      } else {
        // if an older browser, fall back to jQuery animate
        elem.css({ 'left': posX +'px', 'top': posY+'px' });
      }
    }

    $scope.removeBoid = function(boid) {
      boid.clear();
      var idx = -1;
      for (var i = 0; i < $scope.boids.length; i++) {
        if(boid.id == $scope.boids[i].id) {
          idx = i;
          break;
        }
      }

      if(idx != -1) {

        //update parents
        //$scope.boids[idx].family.parents.forEach(function(parent) {
        //  var rmchild = -1;
        //  for (var i = 0; i < parent.family.children.length; i++) {
        //    if(parent.family.children[i].id == boid.id) {
        //      rmchild = i;
        //    }
        //  };
        //  if(rmchild != -1)
        //  {
        //    parent.family.children.splice(rmchild, 1);
        //  }
        //});

        ////update siblings
        //$scope.boid[idx]

        $scope.boids.splice(idx, 1);
      }

      $scope.$broadcast('changeBoids');
    };

    $scope.getBoid = function(id) {

      //console.log("id: ", id);

      //console.log($scope.boids);
      for (var i = 0; i < $scope.boids.length; i++) {
        if($scope.boids[i].id == id) { 
          return $scope.boids[i];
        }
      }
    };

    $scope.startGeneration = function() {

      console.log("START SELECTION PROCESS");
      $scope.message = "Starting Selection Process...";
      var tracks = $scope.boids.map(function(boid){ return boid.track; });

      // FITNESS
      GEN.fitness(tracks).then(function() {
      var userProfile= [{x:0,y:0},{x:1,y:0},{x:2,y:0},{x:3,y:0},{x:4,y:0},{x:5,y:0},{x:6,y:0},{x:7,y:0},{x:8,y:0},
                        {x:9,y:0},{x:10,y:0},{x:11,y:0},{x:12,y:0}];
      var aveAcou=0,
          aveDance=0,
          aveDure=0,
          aveEne=0,
          aveInstr=0,
          aveKey=0,
          aveLiv=0,
          aveLou =0,
          aveMod=0,
          aveSpe=0,
          aveTem =0,
          aveSig=0,
          aveVal=0;

        $scope.currentAlgVis = AlgVis.fitness;
        $scope.message = "Visualizing fitness";
        console.log("called fitnesss().", tracks);
        
        tracks.forEach(function(track) {
          var boid = $scope.getBoid(track.boidId);
          boid.fitness = track.fitness;
          aveAcou += track.profile.acousticness;
          aveDance += track.profile.danceability;
          aveDure += track.profile.duration;
          aveEne += track.profile.energy;
          aveInstr += track.profile.instrumentalness;
          aveKey += track.profile.key;
          aveLiv += track.profile.liveness;
          aveLou += track.profile.loudness;
          aveMod += track.profile.mode;
          aveSpe += track.profile.speechiness;
          aveTem += track.profile.tempo;
          aveSig += track.profile.time_signature;
          aveVal += track.profile.valence;

          // boidProfile[0].y = track.profile.acousticness;
          // boidProfile[1].y = track.profile.danceability;
          // boidProfile[2].y = track.profile.duration;
          // boidProfile[3].y = track.profile.energy;
          // boidProfile[4].y = track.profile.instrumentalness;
          // boidProfile[5].y = track.profile.key;
          // boidProfile[6].y = track.profile.liveness;
          // boidProfile[7].y = track.profile.loudness;
          // boidProfile[8].y = track.profile.mode;
          // boidProfile[9].y = track.profile.speechiness;
          // boidProfile[10].y= track.profile.tempo;
          // boidProfile[11].y= track.profile.time_signature;
          // boidProfile[12].y= track.profile.valence;
          // boid.profil =  stack(d3.range(n).map(function() {return boidProfile;}));
          // console.log("Id : " + track.boidId);
          // console.log(boid.profil);
        });

        userProfile[0].y = aveAcou/tracks.length;
        userProfile[1].y = aveDance/tracks.length;
        userProfile[2].y = aveDure/tracks.length;
        userProfile[3].y = aveEne/tracks.length;
        userProfile[4].y = aveInstr/tracks.length;
        userProfile[5].y = aveKey/tracks.length;
        userProfile[6].y = aveLiv/tracks.length;
        userProfile[7].y = aveLou/tracks.length;
        userProfile[8].y = aveMod/tracks.length;
        userProfile[9].y = aveSpe/tracks.length;
        userProfile[10].y = aveTem/tracks.length;
        userProfile[11].y = aveSig/tracks.length;
        userProfile[12].y = aveVal/tracks.length;

        $scope.userProfile = stack(d3.range(n).map(function() {return userProfile;}));
        background($scope.userProfile);
        
        setTimeout(function() {

          // SELECTION
          GEN.selection(tracks).then(function(result) {
            $scope.currentAlgVis = AlgVis.selection;
            $scope.message = "Visualizing selection";
            console.log("Called selection(). ALIVE: ", result.alive, " DEAD: ", result.dead);
            //kill everyone
            result.dead.forEach(function(kill) {
              var boid = $scope.getBoid(kill.boidId);
              //console.log(boid);
              boid.initializeSelection();
            });
            
            setTimeout(function() {

              //do kill (remove)
              result.dead.forEach(function(kill) {
                  var boid = $scope.getBoid(kill.boidId);
                  $scope.removeBoid(boid);
              });

              // CROSSOVER
              GEN.crossover(result.alive).then(function(crossovers) {
                $scope.currentAlgVis = AlgVis.crossover;
                $scope.message = "Visualizing crossover";
                console.log("Called crossover(). ");
                console.log("crossovers: ", crossovers);
                
                /*
                  first, each parent will initialize the process of crossover.

                  At this point the crossover visualisation will be taken care of in Boid::visualizeCrossover.
                  The routine will end by setting the currentAlgVis to None, after approximately
                  algvizOptions.crossoverVistime time.

                */

                // O(N/2 * N) = O(N^2)
                var crossoverpromises = [];
                crossovers.forEach(function(crossover) {
                  var responsibleParent = true; //spawning responsible
                  

                  //target mean point
                  //var fuzzytarget = $scope.getBoid(crossover.parents[0].boidId).position;
                  //for (var i = 1; i < crossover.parents; i++) {
                  //  fuzzytarget = fuzzytarget.add($scope.getBoid(crossover.parents[i].boidId).position);
                  //};
                  //fuzzytarget = fuzzytarget.divide(crossover.parents.length);
                  var fuzzytarget = paper.Point.random().multiply(paper.view.size);

                  crossover.parents.forEach(function(parenttrack) {
                    var parent = $scope.getBoid(parenttrack.boidId);
                    var r = 1;
                    var target = fuzzytarget.add(paper.Point.random().multiply(r, r).subtract(r/2, r/2));

                    var crossoverpromise = parent.initializeCrossover(crossover, target, responsibleParent);

                    crossoverpromises.push(crossoverpromise);
                    responsibleParent = false;
                  });

                });

                Promise.all(crossoverpromises).then(function() {
                  console.log("ALL CROSSOVER PROMISES DONE");
                  console.log("SELECTION PROCESS DONE");
                  $scope.currentAlgVis = AlgVis.none;
                  $scope.message = "";

                  crossovers.forEach(function(crossover) {
                    crossover.children.forEach(function(childtrack){
                      var child = $scope.getBoid(childtrack.boidId);
                      //add parents and children
                      crossover.parents.forEach(function(parenttrack) {
                        child.family.parents.push(parenttrack.boidId);
                        $scope.getBoid(parenttrack.boidId).family.children.push(child.id);
                      });
                      //add siblings
                      crossover.children.forEach(function(siblingtrack){
                        child.family.siblings.push(siblingtrack.boidId);
                      });
                    });
                  });

                  //console.log("=======================================> ", $scope.boids);

                  $scope.boids.forEach(function(boid) {
                    boid.clearTarget();
                  });

                });
              });

            }, $scope.algvizOptions.selectionVistime);
          });

        }, $scope.algvizOptions.fitnessVistime);
      });
    };

    /* CREATE PLAYLIST */
    $scope.exportPlaylist = function(){

      /* LOTS OF COMMENTS, ASK LENNY */

      console.log("in Export playlist");
      $scope.trackuris = $scope.boids.map(function(boid){return boid.track.uri});
      //console.log($scope.trackuris);

      // API.getPlaylists($scope.username).
      //   then(function(r){
      //
      //       if($.inArray("Genetic Playlist",r.map(function(pl){return pl.name})) == -1){
                API.createPlaylist($scope.username)
                  .then(function(u) {
                     API.addTracksToPlaylist($scope.username,u.id,$scope.trackuris)
                    .then(function(q) {
                       console.log("Genetic Playlist Created");
                       window.location.href = 'spotify:user:'+$scope.username+':playlist:'+u.id;
                    });
                 });
        //     }
        //
        //     else{
        //       console.log("Replacing tracks in Genetic Playlist");
        //       var i = r.map(function(pl){return pl.name}).indexOf("Genetic Playlist");
        //       var id = r[i].id;
        //       //console.log(id);
        //       API.replacePlaylistTracks($scope.username,id,$scope.trackuris).
        //         then(function(t){
        //           console.log("bajs");
        //         });
        //     }
        //
        // });


      
    };


    $scope.mouseUp = function() {

    };

    $scope.mouseDrag = function(event) {
    
    };

    $scope.mouseDown = function(event) {
    
    };

    $scope.onFrame = function(event) {
      for (var i = 0; i < $scope.boids.length; i++) {
        $scope.boids[i].run(event.delta, $scope.boids);
        $scope.boids[i].draw();
      }
    };

    $scope.onResize = function(event) {
        var height = $("#gamediv").height() - $("#gameTopMenu").outerHeight( true );
        var width = $("#gamediv").width();
        paper.view.viewSize = new paper.Size(width, height);
    };

    $scope.addToSimulation = function(tracks) {
      tracks.forEach(function(track) {
        addBoid(track.track);
      });
    };

    $scope.restartSimulation = function() {
      $scope.boids.forEach(function(boid) {
        boid.clear();
      });
      $scope.boids = [];
      $scope.$broadcast('changeBoids');
    };


    /* Events from other controllers */

    $scope.$on('addPlaylist', function(event, playlistId) {
      console.log("addPlaylist event: ", playlistId);
      if(playlistId === undefined) return;

      API.getPlaylistTracks($scope.username, playlistId)
        .then(function(tracks) {
          $scope.addToSimulation(tracks.items);
        });
    });

    $scope.$on('addSong',function(event,song){
      console.log("addSong event: ",song);
      addBoid(song);
    });

    $scope.$on('startGeneration',function(event){
      $scope.startGeneration();
    });

    $scope.$on('exportPlaylist',function(event){
      $scope.exportPlaylist();
    });

    $scope.$on('restart',function(event){
      $scope.restartSimulation();
    });

    $scope.$on('seedPlaylist', function(event, playlistId) {
      console.log("seedPlaylist event: ", playlistId);
      if(playlistId === undefined) return;

        //TODO: prompt "are you sure"
        $scope.restartSimulation();
        API.getPlaylistTracks($scope.username, playlistId)
          .then(function(tracks) {
            $scope.addToSimulation(tracks.items);
         });
    });

    /* Special case from same controller */
      $scope.$on('changeBoids',function(event){
        $scope.trackids = $scope.boids.map(function(boid){return boid.track.id});
        $scope.tracknames = $scope.boids.map(function(boid){return boid.track.name});
        //console.log("in changeboids listener");

        $("#genSongs").empty();
          
  
          for (x in $scope.trackids){
            //console.log(tracks.items[x].track.name);
            $("#genSongs").append("<li class='menuitems'><div id='gen"+$scope.trackids[x]+"' class='genSongName'>"+$scope.tracknames[x]+"</div></li>");
          }
      
        });
      

    /* Initialize Paper.js and initGame*/

    console.log("initializing paper.js");
    paper.install(window);
    paper.setup('game-canvas');
    paper.view.onFrame = $scope.onFrame;
    paper.view.onResize = $scope.onResize;
    $scope.onResize(); //initial resize

    Promise.all(promiseStack).then(function() {
      console.log("promises done. Initializing game");
      initGame();
    });

  });

})();

jQuery.fn.fadeOutAndRemove = function(speed){
    $(this).fadeOut(speed,function(){
        $(this).remove();
    })
}
  


function getColorFromValue(value) {
  var colors = ['#a50026', '#d73027', '#f46d43', '#fdae61', '#fee08b', '#d9ef8b', '#a6d96a', '#66bd63', '#1a9850', '#006837', '#006B39'];
  return colors[truncate(value)];
}

function truncate(value)
{
    if (value < 0)
        return Math.ceil(value);

    return Math.floor(value);
}
