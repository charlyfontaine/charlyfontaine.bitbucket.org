(function() {

	var module = angular.module('PlayerApp');

	module.controller('LoginController', function($scope, Auth) {
		$scope.isLoggedIn = false;

		$scope.login = function() {
			console.log('do login...');
			console.log("location.host: ", location.host);

			Auth.openLogin();
			//$scope.$emit('login');
		}
	});

})();
