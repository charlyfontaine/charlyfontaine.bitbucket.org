(function() {
	var module = angular.module('PlayerApp');

	module.factory('FIT', function(Auth, API, $q) {

	   	return {
	   		fitnessfunc: function(trackids) {
	   			console.log("fitnessfunc")
	   			//console.log(trackids)
	   			
	   			var ret = $q.defer();
	   			
	   			// Load tracks from track id
	   			API.getTracks(trackids).then(function(tracks){

	   				//Calculate current statistics
	   				var duration = tracks.tracks.map(function(d){return d.duration_ms;})
	   				mean_dur = duration.reduce(function(a,b) {return a+b;}) / duration.length;


	   				//Make changes to the songs and kill'em of
	   				var new_tracks = tracks.tracks;

	   				for (i=0; i<tracks.tracks.length; i++) {
	   					if (+tracks.tracks[i].duration_ms > mean_dur){
	   						new_tracks.splice(i--,1);
	   					}
	   				}


	   				//Pack songs for return
					ret.resolve(new_tracks);
				});

	   			return ret.promise;
	   		}
	   	}

	});

})();