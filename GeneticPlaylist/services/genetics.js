(function() {

	var module = angular.module('PlayerApp');


	/*private GEN funtions */
	function fitnessFunction(track, profileVectorPL) {
		var distance = 0;


		//Check and update age
		if (typeof track.age == "undefined"){
			track.age = 1;
		}
		else {
			track.age += 1;
		}


		trackProfVec = playlistProfile([track.profile])
		for (i=0; i<trackProfVec.length; i++){
			distance += Math.pow((trackProfVec[i]-profileVectorPL[i]), 2);
		}

		// calculate fitness
		fitness = (12 - Math.sqrt(distance)) * (1-( Math.pow(track.age*0.09, 2) ))
		if (fitness < 0){fitness = 0;}
		return fitness;
	};

	function playlistProfile(profiles){
		var profileVector = [];
		var acousticness = 0;
		var danceability = 0;
		var duration = 0;
		var energy = 0;
		var instrumentalness = 0;
		var key = 0;
		var liveness = 0;
		var loudness = 0;
		var mode = 0;
		var speechiness = 0;
		var tempo = 0;
		var time_signature = 0;
		var valence = 0;

		// Set parameters from profiles to a vector
		for (i=0; i<profiles.length; i++){
			if (typeof profiles[i].response != "undefined"){
				acousticness += 0;
				danceability += 0;
				duration += 0;
				energy += 0;
				instrumentalness += 0;
				key += 0;
				liveness += 0;
				loudness += 0;
				mode += 0;
				speechiness += 0;
				tempo += 0;
				time_signature += 0;
				valence += 0;
			}
			else {
				acousticness += profiles[i].acousticness;
				danceability += profiles[i].danceability;
				duration += profiles[i].duration;
				energy += profiles[i].energy;
				instrumentalness += profiles[i].instrumentalness;
				key += profiles[i].key;
				liveness += profiles[i].liveness;
				loudness += profiles[i].loudness;
				mode += profiles[i].mode;
				speechiness += profiles[i].speechiness;
				tempo += profiles[i].tempo;
				time_signature += profiles[i].time_signature;
				valence += profiles[i].valence;
			}
		}

		//Manage normalization
		profileVector[0] = acousticness/profiles.length * 10;
		profileVector[1] = danceability/profiles.length * 10;
		profileVector[2] = duration/profiles.length / 25;
		profileVector[3] = energy/profiles.length * 10;
		profileVector[4] = instrumentalness/profiles.length * 10;
		profileVector[5] = key/profiles.length; //The key starts at 0 and goes to ?
		profileVector[6] = liveness/profiles.length * 10;
		profileVector[7] = loudness/profiles.length * -1 / 2;
		profileVector[8] = mode/profiles.length * 10;
		profileVector[9] = speechiness/profiles.length * 10;
		profileVector[10] = tempo/profiles.length / 25;
		profileVector[11] = time_signature/profiles.length / 10;
		profileVector[12] = valence/profiles.length * 10;

		
		return profileVector;
	};

	function getRandom(items) {
		return items[Math.floor(Math.random()*items.length)];
	}

	function cleanupForeign(foreign_id) {
		var prefixlength = 15;
		return foreign_id.slice(prefixlength, foreign_id.length);
	}





	module.factory('GEN', function(Auth, API, FIT, $q) {
		
		/*
				Naive algorithm: 
					- repeat nchildren times:
						- random parent -> random related artist -> random album -> random song

		*/
		function createSingleOffspring(parents) {
			var ret = $q.defer();

			//for (var i = 0; i < nchildren; i++) { //TODO: more than 1 child

			// random parent seed
			var parent = getRandom(parents);
			//console.log("random parent: ", parent.name);

			// random similar artist:
			API.getSimilarArtist(parent.artists[0].id).then(function(relatedRes) {
				var related = getRandom(relatedRes.response.artists);
				//console.log("picked related artist: ", related.name);

				if(!related.hasOwnProperty("foreign_ids")) {
					console.log("no foreign_ids");
					ret.reject();
					return;
				}

				if(!related.foreign_ids.hasOwnProperty("0")) {
					console.log("no foreign_ids[0]");
					ret.reject();
					return;
				}

				var spotify_id = cleanupForeign(related.foreign_ids[0].foreign_id);

				//pick random album from artist
				randomAlbum = API.getArtistAlbums(spotify_id, Auth.getUserCountry())
				randomAlbum.then(function(albums) {
					var album = getRandom(albums.items);
					//console.log("picked random album:", album.name);


					if (typeof album == "undefined"){
						ret.reject();
						return;
					}



					//pick random track
					API.getAlbumTracks(album.id).then(function(tracks) {
						//console.log("tracks: ", tracks);
						var child = getRandom(tracks.items);
						//console.log("picked random track: ", child.name);

						API.getTrack(child.id).then(function(childtrack) {
							//console.log("RESULTING TRACK:", childtrack);
							
							ret.resolve(
								{
									parents: parents, 
									children: [childtrack]
								}
							); //TODO: Several children

						});
					});
				});
			});

			//}

			return ret.promise;
		};


		/* public GEN functions */
		return {

			/*
				Given List<Track> tracks, computes the fitness for
				each track.

				The routine attaches an t_fitness fitness onto each track.
				
				The GEN Options will have a max and min fitness etc...
			*/
			fitness: function(tracks) {

				var ret = $q.defer();
				//Promises
				var promises = [];
				var promisLvl2 = [];
				
				//Example: count the number of tracks
				var aveDur = 0 ;
				var maximum = -10000;
				var profileVector = [];
				var profiles = [];


				//Setup for current playlist
				var j = 0;
				for (var i=0; i<tracks.length; i++){
					if (typeof tracks[i].profile == "undefined"){
						var profilePromise = API.getTrackProfile(tracks[i].id)
					
						promisLvl2.push(profilePromise);
					

						profilePromise.then(function(profile){
							if (typeof tracks[j].profile != "undefined"){//Sort in the profile so that it matches the track
								j++
							}
							tracks[j].profile = profile;
							profiles.push(profile);
							j++
						})
					}
					else {
						profiles.push(tracks[i].profile);
						j++
					}
				}
				
				var P2 = Promise.all(promisLvl2)
				promises.push(P2);

				P2.then(function(d){
					profileVector = playlistProfile(profiles); 
				})

				// Calculate fitness
				Promise.all(promises).then(function() { 
					tracks.forEach(function(track){track.fitness = fitnessFunction(track, profileVector);})


					//*******adding the mainstreamness of the user in account to the fitness
					// mainstreamness is the number of mainstream songs(top 15 hot songs in spotify) divided by the number of songs
					//Fitmainstreamness is equal to 1 + mainstreamness so it will promote mainstreamsongs if the user is mainstream
					//if the song is mainstream then multiply it's fitness by Fitmainstreamness
					//Edit fitness according to the similarity to popular music (top 30 artists)
					var mainstream = 0; //[0,1]
					var mainstreamPromise = API.getHotArtists(30)
					promises.push(mainstreamPromise)
	
					mainstreamPromise.then(function(artist){
						tracks.forEach(function(track) {
							artist.forEach(function(d){
								if (track.artists[0].name == d.name){
									mainstream += 1;
									track.mainstream = true;
								}
							}); 
							if (track.mainstream == undefined){track.mainstream=false}
						});
						mainstream = mainstream / tracks.length;
					});
	

					Promise.all(promises).then(function(){
						var fitmainstreamness = 1 + mainstream ; 
						var unfitMainstreamness = 1 - mainstream ;

						if (+mainstream>0.2){ // the user is mainstream
							console.log("You mainstream madafacka");
							tracks.forEach(function(track) {
								if(track.mainstream== true){
								track.fitness = track.fitness * fitmainstreamness;	// the user is mainstream and each song that is mainstream is promoted
	  							}
	  							else{track.fitness = track.fitness * unfitMainstreamness;}
	  						});
	
							}
						else {
							console.log("You hipster madafacka");
							tracks.forEach(function(track) { // the user is not mainstream
								if(track.mainstream==false){
								track.fitness = track.fitness * fitmainstreamness;	// the user is not mainstream and each song that is not mainstream is promoted
	  							}
	  							else{
	  								track.fitness = track.fitness * unfitMainstreamness;
	  							}
							});
						}

				  	ret.resolve();



				  	});
				});
			

				return ret.promise;
			},


			/*
				Given List<Track> tracks with fitness, computes which tracks are
				selected to breed a new generation.

				The function returns two lists:
					- List<Track> alive
					- List<Track> dead

				OBS: null-check track.fitness
			*/
			
			selection: function(tracks) { //min survival: 20% (min 2 songs if possible) and max 100%
				//var mainstream = 0; //[0,1]
				var promises = [];
				var ret = $q.defer();


				//Calculate mean of fitness
				meanFitness = tracks.map(function(d){return d.fitness})
				var sum = 0;
				for (var i=0; i<meanFitness.length; i++){
					sum += meanFitness[i];
				}
				popAdjust = (20 - tracks.length)/(16 + tracks.length)
				threshold = (sum/meanFitness.length) - popAdjust;
				

				//Do when done
				Promise.all(promises).then(function(d){
					var alive = [];
					var dead = [];

					

					tracks.forEach(function(track){
						if (threshold<track.fitness){
							alive.push(track);
						}
						else {
							dead.push(track)
						};
					})

					//Check so that not all songs die
					while (alive.length<2){
						index = Math.floor(Math.random()*dead.length);
						alive.push(dead[index]);
						dead.splice(index,1);
					}

					var result = { 
						alive:alive, 
						dead:dead
						};
					ret.resolve(result);
				});
				return ret.promise;


			},


			/* Given a list of alive tracks to breed a new generation, generates the next generation
				of tracks. 

				The routine returns an associative array with parents and their generated children:
					- crossovers =  { 
							List<Track> parents: List<Track> children,
							.
							.
							List<Track> parents: List<Track> children
					 	}
			*/
			crossover: function(alive) {
				var ret = $q.defer();
				var crossovers = [];

				//TODO: better options
				var nparent = 2;
				var nchildren = nparent / 2;

				var crossoverpromises = [];

				while(alive.length > 0){

					// select parents in groups of nparent
					var currentparents = [];
					for (var i = 0; i < nparent; i++) {
						var parent = alive.pop();
						if(parent !== undefined) {
							currentparents.push(parent);
						}
					};

					//console.log("curr parents ", currentparents);

					//create children
					var crossoverpromise = createSingleOffspring(currentparents, nchildren);
					crossoverpromises.push(crossoverpromise);

					crossoverpromise.then(function(crossover) {
						crossovers.push(crossover);
					});
					crossoverpromise.catch(function(){ 
						console.log("createSingleOffspring promise failed!");
					});
				}

				Promise.all(crossoverpromises).then(function() {
					ret.resolve(crossovers);
				});
				return ret.promise;
			},


		};
	});

})();
