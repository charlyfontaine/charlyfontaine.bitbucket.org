(function() {


/////////////////// HELP FUNCTIONS ///////////////////
	var module = angular.module('PlayerApp');

	module.factory('API', function(Auth, $q, $http) {

		var spotifyBaseUrl = 'https://api.spotify.com/v1';
		var echoNestUrl = "developer.echonest.com";
		var echoNestApiKey = '2MDZ7OYLJEX62S8SD';


		//Create EchoNest URL
		function each(obj, func) {
    	    var key;
    	    for (key in obj) {
    	        if (obj.hasOwnProperty(key)) {
    	            func.call(obj, key);
    	        }
    	    }
    	}

    	function isArray(obj) {
	        return Object.prototype.toString.apply(obj) === '[object Array]';
	    }


		// return a query string from an object
    	function queryString(params) {
    	    var query = '?', first = true;
    	    var value;
    	    each(params, function (key) {
    	        var i;
    	        // only prepend `&` when this
    	        // isn't the first k/v pair
    	        if (first) {
    	            first = false;
    	        } else {
    	            query += '&';
    	        }
    	        value = params[key];
    	        if (isArray(value)) {
    	            for (i = 0; i < value.length; i += 1) {
    	                query += (encodeURI(key) + '=' + encodeURI(value[i]));
    	                if (i < (value.length - 1)) {
    	                    query += '&';
    	                }
    	            }
    	        } else {
    	            query += (encodeURI(key) + '=' + encodeURI(value));
    	        }
    	    });
    	    return query;
    	}


		function echoGetUrl(type, method, query) {
            query.api_key = echoNestApiKey; //Key for Echo
            query.format = 'json';
            var url = 'http://';
            url += echoNestUrl;
            url += "/api/v4/";
            url += type + '/';
            url += method;
            url += queryString(query);

            return url;
            };



		return {

/////////////////// ECHO NEST API SETUP ////////////////////

			//Input: Spotify-id of artist
			getArtistProfile: function(artist__id) {
				var query =
					{
						id: "spotify:artist:" + artist__id,
						bucket: ["discovery","genre", "artist_location", "familiarity", "hotttnesss", "years_active"]
					}

				type = "artist";
				method = "profile";

				var ret = $q.defer();
				$http.get(echoGetUrl(type, method, query))
				.success(function(r) {
					console.log('got userinfo', r);
					ret.resolve(r);
				}).error(function(err) {
					console.log('failed to get userinfo', err);
					ret.reject(err);
				});
				return ret.promise;
			},

			//Input: Spotify-id of song
			getArtistGenre: function(artist__id) {
				var query = 
					{
						id: "spotify:artist:" + artist__id,
						bucket: ["genre"]
					}

				type = "artist";
				method = "profile";

				var ret = $q.defer();
				$http.get(echoGetUrl(type, method, query))
				.success(function(r) {
					//console.log('got userinfo', r);
					ret.resolve(r.response.artist.genres.map(function(d){return d.name;}));
				}).error(function(err) {
					console.log('failed to get userinfo', err);
					ret.reject(err);
				});
				return ret.promise;
			},


			//Input: Spotify-id of artist
			getSimilarArtist: function(artist__id) {
				var query =
					{
						id: "spotify:artist:" + artist__id,
						bucket:["id:spotify-WW"]
					}

				type = "artist";
				method = "similar";

				var ret = $q.defer();
				var requeststring = echoGetUrl(type, method, query);
				//console.log("request: ", requeststring);
				$http.get(requeststring)
				.success(function(r) {
					//console.log('got related artist info:', r);
					ret.resolve(r);
				}).error(function(err) {
					console.log('failed to get userinfo', err);
					ret.reject(err);
				});
				return ret.promise;
			},


			//Input: Spotify-id of track
			getTrackProfile: function(track__id) {
				var query =
					{
						id: "spotify-WW:track:" + track__id,
						bucket: ["audio_summary"]
					}

				type = "track";
				method = "profile";

				var ret = $q.defer();
				$http.get(echoGetUrl(type, method, query))
				.success(function(r) {

					if (r.response.status.code == 0){
						ret.resolve(r.response.track.audio_summary);
					}
					else {console.log("Not found: ", r); ret.resolve(r);}

				}).error(function(err) {
					console.log('failed to get userinfo', err);
					ret.reject(err);
				});
				return ret.promise;
			},

			//Input: Spotify-id of song
			getTrackType: function(track__id) {
				var query =
					{
						track_id: "spotify-WW:track:" + track__id,
						bucket: ["song_type"] //"tracks", "id:spotify-WW"
					}

				type = "song";
				method = "profile";

				var ret = $q.defer();
				$http.get(echoGetUrl(type, method, query))
				.success(function(r) {
					console.log('got userinfo', r);
					ret.resolve(r.response.songs[0].song_type);
				}).error(function(err) {
					console.log('failed to get userinfo', err);
					ret.reject(err);
				});
				return ret.promise;
			},



			getHotArtists: function(__results, __start) { //
				__start = __start || "0";
				__results = __results || "15";
				var query =
					{
						//track_id: "spotify-WW:track:" + track__id,
						start: __start,
						results: __results
						//bucket: ["hotttnesss"] //"tracks", "id:spotify-WW"
					}

				type = "artist";
				method = "top_hottt";

				var ret = $q.defer();
				$http.get(echoGetUrl(type, method, query))
				.success(function(r) {
					console.log('got userinfo', r);
					ret.resolve(r.response.artists);
				}).error(function(err) {
					console.log('failed to get userinfo', err);
					ret.reject(err);
				});
				return ret.promise;
			},




			creteStaticPlaylist: function(__results) {
				__results = __results || "15";
				var query =
					{
						results: __results,
						type: "artist",
						artist: "radiohead",
						bucket: ["id:spotify-WW", "tracks"]
					}

				type = "playlist";
				method = "static";

				var ret = $q.defer();
				$http.get(echoGetUrl(type, method, query))
				.success(function(r) {
					//console.log('got userinfo', r);
					res = r.response.songs.map(function(d){
						return d.tracks.map(function(d){return d.foreign_id;})
					});
					res = res.map(function(d){return d[0]})

					for(i=0; i<res.length; i++){
						if (res[i] == undefined){
							res.splice(i,1)
							i--;
						}
						else{res[i] = res[i].slice(14, res[i].length);} //.slice(14,-1);}
					}

					ret.resolve(res);
				}).error(function(err) {
					console.log('failed to get userinfo', err);
					ret.reject(err);
				});
				return ret.promise;
			},





/////////////////// SPOTIFY API SETUP ////////////////////
			getMe: function() {
				var ret = $q.defer();
				$http.get(spotifyBaseUrl + '/me', {
					headers: {
						'Authorization': 'Bearer ' + Auth.getAccessToken()
					}
				}).success(function(r) {
					console.log('got userinfo', r);
					ret.resolve(r);
				}).error(function(err) {
					console.log('failed to get userinfo', err);
					ret.reject(err);
				});
				return ret.promise;
			},

			getMyUsername: function() {
				var ret = $q.defer();
				$http.get(spotifyBaseUrl + '/me', {
					headers: {
						'Authorization': 'Bearer ' + Auth.getAccessToken()
					}
				}).success(function(r) {
					console.log('got userinfo', r);
					//ret.resolve(r.id);
					ret.resolve('test_1');
				}).error(function(err) {
					console.log('failed to get userinfo', err);
					//ret.reject(err);
					//
					ret.resolve('test_1');
				});
				return ret.promise;
			},

			getMyTracks: function() {
				var ret = $q.defer();
				$http.get(spotifyBaseUrl + '/me/tracks', {
					headers: {
						'Authorization': 'Bearer ' + Auth.getAccessToken()
					}
				}).success(function(r) {
					console.log('got user tracks', r);
					ret.resolve(r);
				});
				return ret.promise;
			},


			getPlaylistTracksFromHref: function(hrf){
				var ret = $q.defer();
				$http.get(hrf, {
					headers: {
						'Authorization': 'Bearer ' + Auth.getAccessToken()
					}
				}).success(function(r) {
					//console.log('got userinfo', r);
					ret.resolve(r);
				});
				return ret.promise;
			},


			containsUserTracks: function(ids) {
				var ret = $q.defer();
				$http.get(spotifyBaseUrl + '/me/tracks/contains?ids=' + encodeURIComponent(ids.join(',')), {
					headers: {
						'Authorization': 'Bearer ' + Auth.getAccessToken()
					}
				}).success(function(r) {
					console.log('got contains user tracks', r);
					ret.resolve(r);
				});
				return ret.promise;
			},

			addToMyTracks: function(ids) {
				var ret = $q.defer();
				$http.put(spotifyBaseUrl + '/me/tracks?ids=' + encodeURIComponent(ids.join(',')), {}, {
					headers: {
						'Authorization': 'Bearer ' + Auth.getAccessToken()
					}
				}).success(function(r) {
					console.log('got response from adding to my albums', r);
					ret.resolve(r);
				});
				return ret.promise;
			},

			removeFromMyTracks: function(ids) {
				var ret = $q.defer();
				$http.delete(spotifyBaseUrl + '/me/tracks?ids=' + encodeURIComponent(ids.join(',')), {
					headers: {
						'Authorization': 'Bearer ' + Auth.getAccessToken()
					}
				}).success(function(r) {
					console.log('got response from removing from my tracks', r);
					ret.resolve(r);
				});
				return ret.promise;
			},


            createPlaylist: function (username){
            	
            	console.log("in API.CreatePlaylist");

                var ret = $q.defer();
				var url = 'https://api.spotify.com/v1/users/' + username +
					'/playlists';
				$.ajax(url, {
					method: 'POST',
					data: JSON.stringify({
						'name': 'Genetic Playlist',
						'public': true
					}),
					dataType: 'json',
					headers: {
						'Authorization': 'Bearer ' + Auth.getAccessToken(),
						'Content-Type': 'application/json'
					},
					success: function(r) {
						console.log('create playlist response', r);
						ret.resolve(r);
					}
				});
					return ret.promise;

                            //$http.post(spotifyBaseUrl + '/users/' + encodeURIComponent(username) + '/playlists/', {name:'gpl'});
                            
                            //$http.post(spotifyBaseUrl + '/users/' + encodeURIComponent(username) + '/playlists/' + '{playlist_id}' + '/tracks?uris=' + $scope.tracks);
                },

			addTracksToPlaylist: function (username, playlist, tracks) {
			//console.log("in Adding tracks");
			console.log('addTracksToPlaylist', username, playlist, tracks);
			var ret = $q.defer();
				var url = 'https://api.spotify.com/v1/users/' + username +
					'/playlists/' + playlist +
					'/tracks'; // ?uris='+encodeURIComponent(tracks.join(','));
				$.ajax(url, {
					method: 'POST',
					data: JSON.stringify(tracks),
					dataType: 'text',
					headers: {
						'Authorization': 'Bearer ' + Auth.getAccessToken(),
						'Content-Type': 'application/json'
					},
					success: function(r) {
						console.log('add track response', r);
						ret.resolve(r);
					}
				});
				return ret.promise;
			},

			/* NOT WORKING */
			replacePlaylistTracks: function (username, playlist, tracks) {
			//console.log("in Adding tracks");
			console.log('replacingPlaylistTracks', username, playlist, tracks);
			var ret = $q.defer();
				var url = 'https://api.spotify.com/v1/users/' + username +
					'/playlists/' + playlist +
					'/tracks'; // ?uris='+encodeURIComponent(tracks.join(','));
				$.ajax(url, {
					method: 'PUT',
					data: JSON.stringify(tracks),
					dataType: 'text',
					headers: {
						'Authorization': 'Bearer ' + Auth.getAccessToken(),
						'Content-Type': 'application/json'
					},
					success: function(r) {
						console.log('add track response', r);
						ret.resolve(r);
					}
				});
				return ret.promise;
			},
			/* END OF NOT WORKING */
                        
			getPlaylists: function(username) {
				var limit = 50;
				var ret = $q.defer();
				var playlists = [];

				$http.get(spotifyBaseUrl + '/users/' + encodeURIComponent(username) + '/playlists', {
					params: {
						limit: limit
					},
					headers: {
						'Authorization': 'Bearer ' + Auth.getAccessToken()
					}
				}).success(function(r) {
					playlists = playlists.concat(r.items);

					var promises = [],
							total = r.total,
							offset = r.offset;

					while (total > limit + offset) {
						promises.push(
							$http.get(spotifyBaseUrl + '/users/' + encodeURIComponent(username) + '/playlists', {
								params: {
									limit: limit,
									offset: offset + limit
								},
								headers: {
									'Authorization': 'Bearer ' + Auth.getAccessToken()
								}
							})
						);
						offset += limit;
					};

					$q.all(promises).then(function(results) {
						results.forEach(function(result) {
							//for (var i = 0; i < result.data.items.length; i++) {
							//	console.log(result.data.items[i].owner.id);
							//};
							// result.forEach(function(data.items) {
								
							// });
							
							playlists = playlists.concat(result.data.items);

						})
						//console.log('got playlists', playlists);
						ret.resolve(playlists);
					});

				}).error(function(data, status, headers, config) {
					ret.reject(status);
				});
				return ret.promise;
			},

			getPlaylist: function(username, playlist) {
				var ret = $q.defer();
				$http.get(spotifyBaseUrl + '/users/' + encodeURIComponent(username) + '/playlists/' + encodeURIComponent(playlist), {
					headers: {
						'Authorization': 'Bearer ' + Auth.getAccessToken()
					}
				}).success(function(r) {
					//onsole.log('got playlists', r);
					ret.resolve(r);
				});
				return ret.promise;
			},

			getPlaylistTracks: function(username, playlist) {
				var ret = $q.defer();
				$http.get(spotifyBaseUrl + '/users/' + encodeURIComponent(username) + '/playlists/' + encodeURIComponent(playlist) + '/tracks', {
					headers: {
						'Authorization': 'Bearer ' + Auth.getAccessToken()
					}
				}).success(function(tracks) {
					console.log('got playlist tracks', tracks);
					ret.resolve(tracks);
				}).error(function(err) {
					//console.log("playlist tracks error ", err);
					console.log("skipping..");
					ret.resolve({items:[]});
				});
				return ret.promise;
			},

			changePlaylistDetails: function(username, playlist, options) {
				var ret = $q.defer();
				$http.put(spotifyBaseUrl + '/users/' + encodeURIComponent(username) + '/playlists/' + encodeURIComponent(playlist), options, {
					headers: {
						'Authorization': 'Bearer ' + Auth.getAccessToken()
					}
				}).success(function(r) {
					console.log('got response after changing playlist details', r);
					ret.resolve(r);
				});
				return ret.promise;
			},

			removeTrackFromPlaylist: function(username, playlist, track, position) {
				var ret = $q.defer();
				$http.delete(spotifyBaseUrl + '/users/' + encodeURIComponent(username) + '/playlists/' + encodeURIComponent(playlist) + '/tracks',
					{
						data: {
							tracks: [{
								uri: track.uri,
								position: position
							}]
						},
						headers: {
							'Authorization': 'Bearer ' + Auth.getAccessToken()
						}
				}).success(function(r) {
					console.log('remove track from playlist', r);
					ret.resolve(r);
				});
				return ret.promise;
			},

			getTrack: function(trackid) {
				var ret = $q.defer();
				$http.get(spotifyBaseUrl + '/tracks/' + encodeURIComponent(trackid), {
					headers: {
						'Authorization': 'Bearer ' + Auth.getAccessToken()
					}
				}).success(function(r) {
					//console.log('got track', r);
					ret.resolve(r);
				});
				return ret.promise;
			},

			getTracks: function(trackids) {
				var ret = $q.defer();
				$http.get(spotifyBaseUrl + '/tracks/?ids=' + encodeURIComponent(trackids.join(',')), {
					headers: {
						'Authorization': 'Bearer ' + Auth.getAccessToken()
					}
				}).success(function(r) {
					//console.log('got tracks', r);
					ret.resolve(r);
				});
				return ret.promise;
			},

			getAlbum: function(albumid) {
				var ret = $q.defer();
				$http.get(spotifyBaseUrl + '/albums/' + encodeURIComponent(albumid), {
					headers: {
						'Authorization': 'Bearer ' + Auth.getAccessToken()
					}
				}).success(function(r) {
					console.log('got album', r);
					ret.resolve(r);
				});
				return ret.promise;
			},

			getAlbumTracks: function(albumid) {
				var ret = $q.defer();
				$http.get(spotifyBaseUrl + '/albums/' + encodeURIComponent(albumid) + '/tracks', {
					headers: {
						'Authorization': 'Bearer ' + Auth.getAccessToken()
					}
				}).success(function(r) {
					//console.log('got album tracks', r);
					ret.resolve(r);
				});
				return ret.promise;
			},

			getArtist: function(artistid) {
				var ret = $q.defer();
				$http.get(spotifyBaseUrl + '/artists/' + encodeURIComponent(artistid), {
					headers: {
						'Authorization': 'Bearer ' + Auth.getAccessToken()
					}
				}).success(function(r) {
					console.log('got artist', r);
					ret.resolve(r);
				});
				return ret.promise;
			},

			getArtistAlbums: function(artistid, country) {
				var ret = $q.defer();
				$http.get(spotifyBaseUrl + '/artists/' + encodeURIComponent(artistid) + '/albums?country=' + encodeURIComponent(country), {
					headers: {
						'Authorization': 'Bearer ' + Auth.getAccessToken()
					}
				}).success(function(r) {
					//console.log('got artist albums', r);
					ret.resolve(r);
				});
				return ret.promise;
			},

			getArtistTopTracks: function(artistid, country) {
				var ret = $q.defer();
				console.log("toptracks artistid: ", artistid);
				$http.get(spotifyBaseUrl + '/artists/' + encodeURIComponent(artistid) + '/top-tracks?country=' + encodeURIComponent(country), {
					headers: {
						'Authorization': 'Bearer ' + Auth.getAccessToken()
					}
				}).success(function(r) {
					console.log('got artist top tracks', r);
					ret.resolve(r);
				});
				return ret.promise;
			},

			getArtistTopTracksWW: function(artistid) {
				var ret = $q.defer();
				console.log("toptracks artistid: ", artistid);
				$http.get(spotifyBaseUrl + '/artists/' + encodeURIComponent(artistid) + '/top-tracks', {
					headers: {
						'Authorization': 'Bearer ' + Auth.getAccessToken()
					}
				}).success(function(r) {
					console.log('got artist top tracks', r);
					ret.resolve(r);
				});
				return ret.promise;
			},

			getSearchResults: function(query) {
				var ret = $q.defer();
				$http.get(spotifyBaseUrl + '/search?type=track,playlist&q=' + encodeURIComponent(query) + '&market=from_token', {
					headers: {
						'Authorization': 'Bearer ' + Auth.getAccessToken()
					}
				}).success(function(r) {
					console.log('got search results', r);
					ret.resolve(r);
				});
				return ret.promise;
			},

			getNewReleases: function(country) {
				var ret = $q.defer();
				$http.get(spotifyBaseUrl + '/browse/new-releases?country=' + encodeURIComponent(country), {
					headers: {
						'Authorization': 'Bearer ' + Auth.getAccessToken()
					}
				}).success(function(r) {
					console.log('got new releases results', r);
					ret.resolve(r);
				});
				return ret.promise;
			},

			getFeaturedPlaylists: function(country, timestamp) {
				var ret = $q.defer();
				$http.get(spotifyBaseUrl + '/browse/featured-playlists?country=' +
					encodeURIComponent(country) +
					'&timestamp=' + encodeURIComponent(timestamp), {
					headers: {
						'Authorization': 'Bearer ' + Auth.getAccessToken()
					}
				}).success(function(r) {
					console.log('got featured playlists results', r);
					ret.resolve(r);
				});
				return ret.promise;
			},

			getUser: function(username) {
				var ret = $q.defer();
				$http.get(spotifyBaseUrl + '/users/' +
					encodeURIComponent(username),
				{
					headers: {
						'Authorization': 'Bearer ' + Auth.getAccessToken()
					}
				}).success(function(r) {
					console.log('got userinfo', r);
					ret.resolve(r);
				}).error(function(err) {
					console.log('failed to get userinfo', err);
					ret.reject(err);
				});
				return ret.promise;
			},

			isFollowing: function(id, type) {
				var ret = $q.defer();
				$http.get(spotifyBaseUrl + '/me/following/contains?' +
					'type=' + encodeURIComponent(type) +
					'&ids=' + encodeURIComponent(id),
				{
					headers: {
						'Authorization': 'Bearer ' + Auth.getAccessToken()
					}
				}).success(function(r) {
					console.log('got following', r);
					ret.resolve(r);
				}).error(function(err) {
					console.log('failed to get following', err);
					ret.reject(err);
				});

				return ret.promise;
			},

			follow: function(id, type) {
				var ret = $q.defer();
				$http.put(spotifyBaseUrl + '/me/following?' +
					'type=' + encodeURIComponent(type),
				{ ids : [ id ] },
				{ headers: { 'Authorization': 'Bearer ' + Auth.getAccessToken() }
				}).success(function(r) {
					console.log('followed', r);
					ret.resolve(r);
				}).error(function(err) {
					console.log('failed to follow', err);
					ret.reject(err);
				});

				return ret.promise;
			},

			unfollow: function(id, type) {
				var ret = $q.defer();
				$http.delete(spotifyBaseUrl + '/me/following?' +
					'type=' + encodeURIComponent(type),
				{ data: {
						ids: [ id ]
				},
				headers: {
					'Authorization': 'Bearer ' + Auth.getAccessToken()
				}
				}).success(function(r) {
					console.log('unfollowed', r);
					ret.resolve(r);
				}).error(function(err) {
					console.log('failed to unfollow', err);
					ret.reject(err);
				});

				return ret.promise;
			},

			followPlaylist: function(username, playlist) {
				var ret = $q.defer();
				$http.put(spotifyBaseUrl + '/users/' + encodeURIComponent(username) + '/playlists/' +
					encodeURIComponent(playlist) + '/followers',
				{},
				{ headers: { 'Authorization': 'Bearer ' + Auth.getAccessToken() }
				}).success(function(r) {
					console.log('followed playlist', r);
					ret.resolve(r);
				}).error(function(err) {
					console.log('failed to follow playlist', err);
					ret.reject(err);
				});

				return ret.promise;
			},

			unfollowPlaylist: function(username, playlist) {
				var ret = $q.defer();
				$http.delete(spotifyBaseUrl + '/users/' + encodeURIComponent(username) + '/playlists/' +
					encodeURIComponent(playlist) + '/followers',
				{ headers: { 'Authorization': 'Bearer ' + Auth.getAccessToken() }
				}).success(function(r) {
					console.log('unfollowed playlist', r);
					ret.resolve(r);
				}).error(function(err) {
					console.log('failed to unfollow playlist', err);
					ret.reject(err);
				});

				return ret.promise;
			},

			isFollowingPlaylist: function(username, playlist) {
				var ret = $q.defer();
				$http.get(spotifyBaseUrl + '/users/' + encodeURIComponent(username) + '/playlists/' +
					encodeURIComponent(playlist) + '/followers/contains', {
						params: {
							ids: [Auth.getUsername()]
						},
						headers: { 'Authorization': 'Bearer ' + Auth.getAccessToken()
					}
				}).success(function(r) {
					console.log('check if playlist is followed', r);
					ret.resolve(r);
				}).error(function(err) {
					console.log('failed to check if playlist is followed', err);
					ret.reject(err);
				});

				return ret.promise;
			}
		};
	});

})();
